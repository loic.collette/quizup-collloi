-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Hôte : 127.0.0.1:3306
-- Généré le :  sam. 15 juin 2019 à 08:09
-- Version du serveur :  10.2.23-MariaDB
-- Version de PHP :  7.2.18

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données :  `u218041964_quiz`
--

-- --------------------------------------------------------

--
-- Structure de la table `bloquer`
--

CREATE TABLE `bloquer` (
  `idProfil` int(11) NOT NULL,
  `idProfil_1` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Déchargement des données de la table `bloquer`
--

INSERT INTO `bloquer` (`idProfil`, `idProfil_1`) VALUES
(11, 7),
(13, 1),
(15, 12);

-- --------------------------------------------------------

--
-- Structure de la table `categorie`
--

CREATE TABLE `categorie` (
  `idCategorie` int(11) NOT NULL,
  `libelleCategorie` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Déchargement des données de la table `categorie`
--

INSERT INTO `categorie` (`idCategorie`, `libelleCategorie`) VALUES
(1, 'Sciences'),
(2, 'Sport'),
(3, 'Série'),
(4, 'Informatique'),
(5, 'Babouche');

-- --------------------------------------------------------

--
-- Structure de la table `chat_msg`
--

CREATE TABLE `chat_msg` (
  `idChatMsg` int(11) NOT NULL,
  `timestampMsg` datetime NOT NULL,
  `contenu` varchar(2048) NOT NULL,
  `lu` tinyint(1) NOT NULL,
  `idProfil_recepteur` int(11) NOT NULL,
  `idProfil_emetteur` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Déchargement des données de la table `chat_msg`
--

INSERT INTO `chat_msg` (`idChatMsg`, `timestampMsg`, `contenu`, `lu`, `idProfil_recepteur`, `idProfil_emetteur`) VALUES
(1, '2019-05-16 18:43:51', 'test', 0, 3, 4),
(2, '2019-05-16 18:54:15', 'Je t\'aime ! &lt;3 ', 1, 2, 1),
(3, '2019-05-16 18:55:22', 'Moi aussi je t\'aime &lt;3 ', 1, 1, 2),
(4, '2019-05-16 18:55:31', 'Love You ', 1, 1, 2),
(5, '2019-05-16 18:55:37', 'Haha &lt;3', 1, 2, 1),
(6, '2019-05-16 18:55:43', '&lt;3\n&lt;3\n&lt;3\n&lt;3\n&lt;3\n&lt;3\n&lt;3\n&lt;3\n&lt;3\n&lt;3\n&lt;3\n&lt;3\n&lt;3', 1, 1, 2),
(7, '2019-05-16 18:55:55', 'Tu me manques &lt;3', 1, 1, 2),
(8, '2019-05-16 19:46:18', 'Tu pues ! :D', 0, 2, 1),
(9, '2019-05-16 20:03:48', 'Coucou', 1, 1, 6),
(10, '2019-05-16 20:04:30', 'hello !', 1, 6, 1),
(11, '2019-05-17 06:31:30', 'bisous sur ta couille', 0, 2, 1),
(12, '2019-05-18 16:01:53', 'Coucou', 0, 6, 14),
(13, '2019-05-19 16:09:44', 'Salut Turlubabouche, ça va ?\n', 0, 14, 15),
(14, '2019-05-19 16:09:50', 'Moi ça va en tout cas', 0, 14, 15),
(15, '2019-05-19 16:10:36', '&lt;?php \n$i=1;\nwhile($i==1){$i=1;}\n?&gt;', 0, 14, 15);

-- --------------------------------------------------------

--
-- Structure de la table `integrer`
--

CREATE TABLE `integrer` (
  `idPartie` int(11) NOT NULL,
  `idQuestion` int(11) NOT NULL,
  `numero` tinyint(4) NOT NULL,
  `ordreReponses` char(4) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Déchargement des données de la table `integrer`
--

INSERT INTO `integrer` (`idPartie`, `idQuestion`, `numero`, `ordreReponses`) VALUES
(1, 1, 2, 'bacd'),
(1, 2, 1, 'dabc'),
(1, 3, 3, 'acdb'),
(1, 4, 5, 'adbc'),
(1, 5, 6, 'adcb'),
(1, 6, 0, 'badc'),
(1, 7, 4, 'cdab'),
(2, 1, 6, 'dabc'),
(2, 2, 1, 'acdb'),
(2, 3, 5, 'dbac'),
(2, 4, 2, 'bdac'),
(2, 5, 4, 'acdb'),
(2, 6, 0, 'cbad'),
(2, 7, 3, 'dcba'),
(3, 1, 3, 'dabc'),
(3, 2, 5, 'adbc'),
(3, 3, 1, 'abdc'),
(3, 4, 2, 'bcda'),
(3, 5, 0, 'dbac'),
(3, 6, 4, 'adbc'),
(3, 7, 6, 'bdac'),
(4, 8, 4, 'dcba'),
(4, 9, 2, 'cabd'),
(4, 10, 3, 'abdc'),
(4, 11, 6, 'cbad'),
(4, 12, 0, 'cbda'),
(4, 14, 5, 'dbac'),
(4, 15, 1, 'dcab'),
(5, 8, 2, 'cdba'),
(5, 9, 4, 'cbad'),
(5, 10, 6, 'bcda'),
(5, 11, 3, 'dacb'),
(5, 12, 0, 'acdb'),
(5, 14, 1, 'acdb'),
(5, 15, 5, 'dcab'),
(6, 1, 1, 'badc'),
(6, 2, 5, 'cdab'),
(6, 3, 3, 'abdc'),
(6, 4, 2, 'bdac'),
(6, 5, 4, 'abdc'),
(6, 6, 0, 'acbd'),
(6, 7, 6, 'adcb'),
(7, 1, 5, 'cdba'),
(7, 2, 4, 'dcba'),
(7, 3, 3, 'cdba'),
(7, 4, 2, 'acbd'),
(7, 5, 1, 'bdca'),
(7, 6, 6, 'cbda'),
(7, 7, 0, 'abcd'),
(8, 1, 6, 'cbda'),
(8, 2, 5, 'abcd'),
(8, 3, 2, 'abdc'),
(8, 4, 1, 'bdca'),
(8, 5, 3, 'bdac'),
(8, 6, 4, 'adbc'),
(8, 7, 0, 'cdab'),
(9, 1, 1, 'dbca'),
(9, 2, 4, 'dbac'),
(9, 3, 2, 'dbca'),
(9, 4, 5, 'cdab'),
(9, 5, 3, 'cdab'),
(9, 7, 0, 'cadb'),
(9, 17, 6, 'bdac'),
(10, 2, 2, 'abdc'),
(10, 3, 4, 'bdca'),
(10, 4, 1, 'cdab'),
(10, 5, 6, 'cadb'),
(10, 6, 5, 'bacd'),
(10, 7, 0, 'cabd'),
(10, 17, 3, 'cbda'),
(11, 8, 1, 'cadb'),
(11, 9, 4, 'dabc'),
(11, 10, 3, 'bacd'),
(11, 11, 6, 'badc'),
(11, 12, 5, 'adbc'),
(11, 14, 0, 'dacb'),
(11, 15, 2, 'dabc'),
(12, 1, 3, 'dcab'),
(12, 3, 2, 'badc'),
(12, 4, 0, 'cabd'),
(12, 5, 4, 'adbc'),
(12, 6, 1, 'dbac'),
(12, 7, 6, 'dbac'),
(12, 17, 5, 'bcda'),
(13, 1, 4, 'cdab'),
(13, 2, 0, 'cadb'),
(13, 4, 2, 'abdc'),
(13, 5, 6, 'badc'),
(13, 6, 5, 'cbad'),
(13, 7, 3, 'adcb'),
(13, 17, 1, 'bcda'),
(14, 1, 2, 'dcba'),
(14, 2, 5, 'bcad'),
(14, 3, 3, 'dbca'),
(14, 4, 0, 'dcab'),
(14, 5, 1, 'abcd'),
(14, 6, 6, 'cadb'),
(14, 17, 4, 'adcb'),
(15, 1, 3, 'abdc'),
(15, 2, 0, 'dcab'),
(15, 3, 2, 'cadb'),
(15, 4, 6, 'dbca'),
(15, 5, 1, 'cbad'),
(15, 7, 4, 'dbac'),
(15, 17, 5, 'bacd'),
(16, 8, 6, 'bcda'),
(16, 9, 4, 'cabd'),
(16, 10, 3, 'cdab'),
(16, 11, 1, 'dbac'),
(16, 12, 0, 'acdb'),
(16, 14, 2, 'badc'),
(16, 15, 5, 'bdca'),
(17, 1, 4, 'bcda'),
(17, 3, 1, 'dcab'),
(17, 4, 2, 'bcad'),
(17, 5, 5, 'abcd'),
(17, 6, 3, 'adbc'),
(17, 7, 0, 'cbad'),
(17, 17, 6, 'cdab'),
(18, 19, 2, 'dbca'),
(18, 20, 0, 'dcab'),
(18, 21, 3, 'bacd'),
(18, 22, 5, 'bcda'),
(18, 23, 1, 'cbda'),
(18, 24, 6, 'abdc'),
(18, 25, 4, 'abdc'),
(19, 19, 5, 'cadb'),
(19, 20, 0, 'dabc'),
(19, 21, 4, 'dcab'),
(19, 22, 3, 'dbca'),
(19, 23, 1, 'bcda'),
(19, 24, 6, 'acbd'),
(19, 25, 2, 'adcb'),
(20, 19, 4, 'bcda'),
(20, 20, 5, 'adcb'),
(20, 21, 0, 'badc'),
(20, 22, 3, 'cbad'),
(20, 23, 2, 'adcb'),
(20, 24, 1, 'abcd'),
(20, 25, 6, 'cabd'),
(21, 19, 2, 'bdca'),
(21, 20, 4, 'dcba'),
(21, 21, 5, 'cabd'),
(21, 22, 0, 'bcda'),
(21, 23, 6, 'bdac'),
(21, 24, 1, 'dcab'),
(21, 25, 3, 'dbca'),
(22, 1, 6, 'dcba'),
(22, 2, 5, 'cbda'),
(22, 3, 0, 'dabc'),
(22, 4, 1, 'cbad'),
(22, 5, 4, 'cadb'),
(22, 7, 2, 'acdb'),
(22, 17, 3, 'bcda'),
(23, 2, 6, 'cbad'),
(23, 3, 1, 'badc'),
(23, 4, 2, 'abdc'),
(23, 5, 5, 'dbac'),
(23, 6, 4, 'dbac'),
(23, 7, 0, 'bacd'),
(23, 17, 3, 'dacb'),
(24, 26, 5, 'acdb'),
(24, 29, 3, 'dabc'),
(24, 30, 0, 'abcd'),
(24, 32, 1, 'dbca'),
(24, 33, 2, 'cabd'),
(24, 34, 4, 'acbd'),
(24, 35, 6, 'abcd'),
(25, 26, 5, 'cdab'),
(25, 27, 3, 'adcb'),
(25, 31, 1, 'dbca'),
(25, 32, 4, 'dcba'),
(25, 33, 2, 'dabc'),
(25, 34, 0, 'cadb'),
(25, 35, 6, 'bdac'),
(26, 26, 4, 'acdb'),
(26, 27, 2, 'dbca'),
(26, 28, 1, 'bacd'),
(26, 29, 3, 'dbac'),
(26, 30, 6, 'bcad'),
(26, 32, 0, 'bacd'),
(26, 34, 5, 'dcab'),
(27, 26, 4, 'cbad'),
(27, 28, 1, 'dacb'),
(27, 30, 6, 'badc'),
(27, 32, 2, 'dbac'),
(27, 33, 5, 'acdb'),
(27, 34, 0, 'cabd'),
(27, 35, 3, 'badc'),
(28, 19, 5, 'cbad'),
(28, 20, 2, 'cbda'),
(28, 21, 0, 'dacb'),
(28, 22, 1, 'bacd'),
(28, 23, 3, 'adbc'),
(28, 24, 6, 'bdca'),
(28, 25, 4, 'cabd'),
(29, 1, 5, 'adcb'),
(29, 3, 2, 'dcab'),
(29, 4, 4, 'adcb'),
(29, 5, 1, 'dacb'),
(29, 6, 6, 'acbd'),
(29, 7, 0, 'dabc'),
(29, 17, 3, 'dabc'),
(30, 28, 3, 'dacb'),
(30, 29, 6, 'badc'),
(30, 30, 0, 'cdab'),
(30, 31, 4, 'dcba'),
(30, 32, 2, 'bcad'),
(30, 33, 5, 'cbad'),
(30, 35, 1, 'adcb'),
(31, 26, 2, 'cdab'),
(31, 27, 4, 'badc'),
(31, 30, 5, 'dcab'),
(31, 31, 3, 'cbad'),
(31, 32, 0, 'adcb'),
(31, 33, 6, 'abcd'),
(31, 35, 1, 'bdac'),
(32, 1, 4, 'dbac'),
(32, 2, 2, 'cabd'),
(32, 3, 6, 'bcad'),
(32, 4, 3, 'dcba'),
(32, 5, 0, 'dbca'),
(32, 7, 5, 'badc'),
(32, 17, 1, 'adbc'),
(33, 1, 6, 'acbd'),
(33, 2, 2, 'dcab'),
(33, 3, 1, 'bdca'),
(33, 4, 0, 'dbac'),
(33, 6, 5, 'cabd'),
(33, 7, 3, 'bcad'),
(33, 17, 4, 'abcd'),
(34, 2, 5, 'acbd'),
(34, 3, 1, 'abcd'),
(34, 4, 0, 'abcd'),
(34, 5, 3, 'dacb'),
(34, 6, 2, 'dcab'),
(34, 7, 6, 'cdab'),
(34, 17, 4, 'cbda'),
(35, 26, 4, 'badc'),
(35, 27, 6, 'abcd'),
(35, 30, 0, 'cbda'),
(35, 31, 5, 'cdab'),
(35, 32, 3, 'cdba'),
(35, 33, 1, 'abcd'),
(35, 35, 2, 'badc'),
(36, 19, 3, 'dcba'),
(36, 20, 5, 'acbd'),
(36, 21, 4, 'dacb'),
(36, 22, 6, 'dacb'),
(36, 23, 2, 'adcb'),
(36, 24, 1, 'dcab'),
(36, 25, 0, 'cadb'),
(37, 8, 5, 'acbd'),
(37, 9, 1, 'bdca'),
(37, 10, 4, 'bdca'),
(37, 11, 0, 'dbac'),
(37, 12, 2, 'badc'),
(37, 14, 6, 'dcba'),
(37, 15, 3, 'dabc'),
(38, 19, 0, 'dbca'),
(38, 20, 1, 'cdab'),
(38, 21, 2, 'abdc'),
(38, 22, 4, 'abcd'),
(38, 23, 5, 'cadb'),
(38, 24, 3, 'bdca'),
(38, 25, 6, 'bcda'),
(39, 1, 1, 'bcad'),
(39, 2, 2, 'cbda'),
(39, 3, 6, 'dcab'),
(39, 4, 5, 'dabc'),
(39, 5, 4, 'badc'),
(39, 6, 3, 'badc'),
(39, 7, 0, 'cdab');

-- --------------------------------------------------------

--
-- Structure de la table `liker`
--

CREATE TABLE `liker` (
  `idProfil` int(11) NOT NULL,
  `idMessage` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Déchargement des données de la table `liker`
--

INSERT INTO `liker` (`idProfil`, `idMessage`) VALUES
(1, 1),
(1, 2),
(1, 3),
(1, 4),
(8, 1),
(13, 4);

-- --------------------------------------------------------

--
-- Structure de la table `message`
--

CREATE TABLE `message` (
  `idMessage` int(11) NOT NULL,
  `timestampMessage` datetime NOT NULL,
  `contenuMessage` varchar(2048) NOT NULL,
  `idMessage_1` int(11) DEFAULT NULL,
  `idTheme` int(11) DEFAULT NULL,
  `idProfil` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Déchargement des données de la table `message`
--

INSERT INTO `message` (`idMessage`, `timestampMessage`, `contenuMessage`, `idMessage_1`, `idTheme`, `idProfil`) VALUES
(1, '2019-05-17 06:30:34', 'salut', NULL, 1, 1),
(2, '2019-05-17 10:10:08', 'sal', 1, 1, 8),
(3, '2019-05-17 10:27:10', 'Salut ! ;)', 2, 1, 1),
(4, '2019-05-19 19:19:43', 'coucou', NULL, 6, 13),
(5, '2019-05-25 09:20:48', 'bonjour', 4, 6, 1);

-- --------------------------------------------------------

--
-- Structure de la table `participer`
--

CREATE TABLE `participer` (
  `idProfil` int(11) NOT NULL,
  `idPartie` int(11) NOT NULL,
  `bonusBoost` decimal(2,1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Déchargement des données de la table `participer`
--

INSERT INTO `participer` (`idProfil`, `idPartie`, `bonusBoost`) VALUES
(1, 1, '1.2'),
(1, 4, '1.2'),
(1, 13, '1.2'),
(1, 15, '1.2'),
(1, 16, '1.2'),
(1, 18, '1.2'),
(1, 19, '1.2'),
(1, 23, '1.2'),
(1, 24, '1.2'),
(1, 25, '1.2'),
(1, 26, '1.2'),
(1, 27, '1.2'),
(1, 28, '1.2'),
(1, 32, '1.0'),
(1, 33, '1.0'),
(1, 34, '1.0'),
(1, 35, '1.0'),
(1, 39, '1.0'),
(2, 2, '1.0'),
(2, 3, '1.0'),
(2, 5, '1.0'),
(6, 6, '1.0'),
(8, 14, '1.0'),
(8, 20, '1.0'),
(9, 7, '1.0'),
(9, 8, '1.0'),
(9, 9, '1.0'),
(10, 10, '1.0'),
(10, 11, '1.0'),
(10, 12, '1.0'),
(11, 17, '1.0'),
(12, 21, '1.0'),
(12, 22, '1.0'),
(15, 29, '1.0'),
(16, 30, '1.0'),
(16, 31, '1.0'),
(17, 36, '1.0'),
(17, 37, '1.0'),
(17, 38, '1.0');

-- --------------------------------------------------------

--
-- Structure de la table `partie`
--

CREATE TABLE `partie` (
  `idPartie` int(11) NOT NULL,
  `timestampPartie` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Déchargement des données de la table `partie`
--

INSERT INTO `partie` (`idPartie`, `timestampPartie`) VALUES
(1, '2019-05-16 18:41:12'),
(2, '2019-05-16 18:51:45'),
(3, '2019-05-16 18:53:22'),
(4, '2019-05-16 19:42:42'),
(5, '2019-05-16 19:47:22'),
(6, '2019-05-16 20:01:09'),
(7, '2019-05-16 22:21:31'),
(8, '2019-05-16 22:22:23'),
(9, '2019-05-16 22:25:42'),
(10, '2019-05-16 22:31:22'),
(11, '2019-05-16 22:32:50'),
(12, '2019-05-16 22:33:34'),
(13, '2019-05-17 05:38:57'),
(14, '2019-05-17 10:11:22'),
(15, '2019-05-17 10:31:36'),
(16, '2019-05-17 10:32:35'),
(17, '2019-05-17 13:00:45'),
(18, '2019-05-17 16:33:55'),
(19, '2019-05-17 16:36:38'),
(20, '2019-05-17 17:41:11'),
(21, '2019-05-18 08:59:39'),
(22, '2019-05-18 09:01:36'),
(23, '2019-05-18 17:40:31'),
(24, '2019-05-19 15:11:14'),
(25, '2019-05-19 15:19:46'),
(26, '2019-05-19 15:21:43'),
(27, '2019-05-19 15:22:09'),
(28, '2019-05-19 15:24:02'),
(29, '2019-05-19 16:11:58'),
(30, '2019-05-19 17:29:20'),
(31, '2019-05-19 17:31:03'),
(32, '2019-05-20 11:16:28'),
(33, '2019-05-20 11:18:51'),
(34, '2019-05-20 11:19:21'),
(35, '2019-05-20 11:20:43'),
(36, '2019-05-20 16:17:26'),
(37, '2019-05-20 16:19:50'),
(38, '2019-05-20 16:20:58'),
(39, '2019-06-14 21:29:21');

-- --------------------------------------------------------

--
-- Structure de la table `pays`
--

CREATE TABLE `pays` (
  `idPays` int(11) NOT NULL,
  `libellePays` varchar(50) NOT NULL,
  `drapeauPays` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Structure de la table `profil`
--

CREATE TABLE `profil` (
  `idProfil` int(11) NOT NULL,
  `nomProfil` varchar(50) NOT NULL,
  `photoProfil` varchar(50) DEFAULT NULL,
  `photoFacade` varchar(50) DEFAULT NULL,
  `villeOrigine` varchar(50) DEFAULT NULL,
  `langue` varchar(20) NOT NULL,
  `bio` varchar(255) DEFAULT NULL,
  `profilPrive` tinyint(1) DEFAULT NULL,
  `diamants` int(11) NOT NULL,
  `idTitre` int(11) DEFAULT NULL,
  `idPays` int(11) DEFAULT NULL,
  `idRegion` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Déchargement des données de la table `profil`
--

INSERT INTO `profil` (`idProfil`, `nomProfil`, `photoProfil`, `photoFacade`, `villeOrigine`, `langue`, `bio`, `profilPrive`, `diamants`, `idTitre`, `idPays`, `idRegion`) VALUES
(1, 'loic2665', '/img/users/profile/1.jpg', '/img/users/facade/1.png', 'Liège', 'Français', 'C\'est moiii ! :D', 0, 50, NULL, NULL, NULL),
(2, 'thelo819', NULL, NULL, 'Bali', 'Français', 'J\'ai vécu Bali. Dans un moment j\'ai vu la vie de mes amis partir et après je vois les étoiles.', 0, 100, NULL, NULL, NULL),
(3, 'plouf', '/img/users/profile/3.jpg', '/img/users/facade/3.jpg', 'Marneffe', 'Français', 'Vous savez, moi je ne crois pas qu’il y ait de bonne ou de mauvaise situation. Moi, si je devais résumer ma vie aujourd’hui avec vous, je dirais que c’est d’abord des rencontres. Des gens qui m’ont tendu la main, peut-être à un moment où je ne pouvais pas', 0, 50, NULL, NULL, NULL),
(4, 'Skzz', NULL, '/img/users/facade/4.jpg', 'Aywaille', 'Français', '', 0, 50, NULL, NULL, NULL),
(5, 'eeeeeeee', NULL, NULL, NULL, 'Français', NULL, NULL, 50, NULL, NULL, NULL),
(6, 'Hydro', NULL, NULL, NULL, 'Français', NULL, NULL, 50, NULL, NULL, NULL),
(7, 'Cira254', NULL, NULL, NULL, 'Français', NULL, NULL, 50, NULL, NULL, NULL),
(8, 'xeyibulege', NULL, NULL, NULL, 'Anglais', NULL, NULL, 50, NULL, NULL, NULL),
(9, 'MartzBlav', NULL, NULL, NULL, 'Français', NULL, NULL, 50, NULL, NULL, NULL),
(10, 'weynrom', NULL, NULL, NULL, 'Français', NULL, NULL, 50, NULL, NULL, NULL),
(11, 'dicker', NULL, '/img/users/facade/11.jpg', 'liege', 'Français', 'zadsefrgazertyuiopqsdfghjklmwxcvbn', 0, 50, NULL, NULL, NULL),
(12, 'Trimix', NULL, NULL, NULL, 'Français', NULL, NULL, 50, NULL, NULL, NULL),
(13, 'hydroblade', NULL, NULL, NULL, 'Français', NULL, NULL, 50, NULL, NULL, NULL),
(14, 'turlubabouche', NULL, NULL, NULL, 'Français', NULL, NULL, 50, NULL, NULL, NULL),
(15, 'Kouristoll', NULL, NULL, NULL, 'Français', NULL, NULL, 50, NULL, NULL, NULL),
(16, 'eltherese', NULL, NULL, NULL, 'Français', NULL, NULL, 50, NULL, NULL, NULL),
(17, 'JTSSNT', NULL, NULL, '', 'Français', '', 1, 50, NULL, NULL, NULL),
(18, 'loicloicloic', NULL, NULL, NULL, 'Français', NULL, NULL, 50, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Structure de la table `question`
--

CREATE TABLE `question` (
  `idQuestion` int(11) NOT NULL,
  `Illustration` varchar(50) DEFAULT NULL,
  `libelleQuestion` varchar(512) NOT NULL,
  `answer` varchar(255) NOT NULL,
  `distracteur01` varchar(255) NOT NULL,
  `distracteur02` varchar(255) NOT NULL,
  `distracteur03` varchar(255) NOT NULL,
  `idTheme` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Déchargement des données de la table `question`
--

INSERT INTO `question` (`idQuestion`, `Illustration`, `libelleQuestion`, `answer`, `distracteur01`, `distracteur02`, `distracteur03`, `idTheme`) VALUES
(1, NULL, 'Quel est la masse molaire de l\'eau ?', '18 g/mol', '14 g/mol', '19 g/mol', '18 kg/mol', 1),
(2, NULL, 'Lequel de ces pH est un acide ?', '4', '7', '9', '17', 1),
(3, NULL, 'A quel élément correspond l\'abréviation  K', 'Potassium', 'Sodium', 'Azote', 'Krypton', 1),
(4, NULL, 'Quel est le plus gros élément présent naturellement en masse molaire ?', 'Uranium', 'Plomb', 'Hydrogène', 'Plutonium', 1),
(5, NULL, 'De quoi est composé le diamant ?', '100% de carbone', '100% de verre', '50% carbone / 50% verre', '100% diamantine', 1),
(6, NULL, 'En quel Ion s\'oxyde le plus facilement le Barium ?', 'BaO', 'BaO2', 'BaO4', 'Ba', 1),
(7, NULL, 'Quelle forme de réaction à lieu dans un réacteur nucléaire ?', 'Fission', 'Fusion', 'Combustion', 'Dissolution', 1),
(8, NULL, 'En quelle année à eu lieu la 1ère édition des jeux olympiques moderne ? ', '1896', '1900', '1920', '1890', 2),
(9, NULL, 'Combien de sport y avait-il dans l\'édition de 2016 ?', '28', '36', '61', '54', 2),
(10, NULL, 'Combien de temps dure un cycle olympique ?', '4 ans', '2 ans', '8 ans', '3 ans', 2),
(11, NULL, 'Qui a inventé les Jeux moderne ?', 'Charles Pierre Fredy de Coubertin', 'Les Dieux Grecques depuis le Mont Olympe', 'Charles-Henri de la Sauvenière', 'Marie Curie', 2),
(12, NULL, 'Où et quand ont eu lieu les Jeux Olympiques lors de l\'édition belge ?', 'A Anvers en 1920', 'Jamais', 'A Liège en 1960', 'A Bruxelles en 58', 2),
(13, NULL, 'Comment s\'appelle le frère de Daryl Dixon ?', 'Merle', 'Merl', 'John', 'Mearl', 3),
(14, NULL, 'Où a eu lieu la première édition des Jeux Olympiques d\'Hiver en 1924 ?', 'Chamonix (le Mont Blanc c\'est très haut)', 'Vancouver', 'Genève', 'Lausane', 2),
(15, NULL, 'Dans quelle ville à eu lieu la première édition des jeux paralympiques de 1960 ?', 'Rome', 'Paris', 'Liège', 'Atlanta', 2),
(16, NULL, 'Qui va voir une sorcière étant petite fille ?', 'Cersei', 'Daenerys', 'Missandei', 'Sansa', 4),
(17, NULL, 'Quel est l\'élément à la base de la chimie organique ?', 'Le Carbone', 'L\'hydrogène', 'L\'Oxygène', 'L\'Hélium', 1),
(19, NULL, 'Quelle structure de données permet un accès aléatoire à ses elements?', 'Tableau', 'Liste simplement liée', 'Liste doublement liée', 'Arbre binaire', 5),
(20, NULL, 'Quel est le pire langage de programmation?', 'C', 'PHP', 'Python', 'Brainfuck', 5),
(21, NULL, 'En C++, quelle est la différence entre le mot clé &quot;class&quot; et &quot;struct&quot;', 'Dans une structure tous les membres ont une visibilité publique par défaut, tandis que dans une classe ils ont une visibilité privée.', 'Aucune différence, les mots peuvent s\'interchanger.', 'Dans une classe on peut définir un constructeur, tandis que dans une structure on ne peut pas.', 'Dans une structure on ne peut pas définir des méthodes.', 5),
(22, NULL, 'Que vaut 0 dans une condition ? if (0) { ... }', 'False / Faux', 'Vide', 'True / Vrai', 'Segmentation Fault', 5),
(23, '/img/questions/5cdee0c00bab0.png', 'Quelle est la bonne manière d\'écrire ce bout de code en C ?', 'C', 'A', 'B', 'D', 5),
(24, NULL, 'Que fait l\'instruction  : MOV AX, 5', 'Met 5 dans le registre AX', 'Met 5 dans la variable AX', 'Met AX dans le registre 5', 'Met 5 dans MOV', 5),
(25, NULL, 'Quelle est la première chose que l\'on essaye quand on apprends un nouveau langage ?', 'Afficher: Hello World!', 'Afficher: test', 'Afficher: coucou', 'Afficher: Hello Wrold!', 5),
(26, NULL, 'Quel acteur jour le rôle de Columbo ?', 'Peter Falk', 'Horst Tappert', 'David Suchet', 'Albert Finney', 6),
(27, NULL, 'Quel est le grade de Columbo ?', 'Lieutenant', 'Sergent', 'Capitaine', 'Commandant', 6),
(28, NULL, 'Quel costume porte toujours Columbo ?', 'Un imperméable et un cigare', 'Un gilet et une casquette', 'Un chapeau et une veste noire', 'Un t-shirt et un jeans', 6),
(29, NULL, 'Quel est le prénom de Columbo ?', 'Frank', 'Peter', 'Rober', 'Michael', 6),
(30, NULL, 'Quelle est la race du chien de Columbo ?', 'Un basset', 'Un caniche', 'Un labrador', 'Un russel', 6),
(31, NULL, 'Comment s\'appelle le chien de Columbo ?', 'Le chien', 'Alfred', 'Médor', 'Louis', 6),
(32, NULL, 'Quelle est la marque de voiture de Columbo ?', 'Peugeot', 'Citroën', 'Volkswagen', 'Dacia', 6),
(33, NULL, 'Quel air de musique Columbo siffle-t-il régulièrement ?', 'This Old Man', 'Oh when the saints', 'Old McDonald', 'I\'m a big man', 6),
(34, NULL, 'Quel chanteur de country joue le rôle du meurtrier dans un épisode ?', 'Johnny Cash', 'Bob Dylan', 'Bill Monroe', 'Il y a pas de meurtrier', 6),
(35, NULL, 'Dans l\'épisode : *L\'Enterrement de Madame Columbo* :  comment la femme de Columbo est-elle censée avoir perdu la vie ?', 'Par empoisonnement', 'Dans un accident de la route', 'Par étranglement', 'Par suicide', 6);

-- --------------------------------------------------------

--
-- Structure de la table `region`
--

CREATE TABLE `region` (
  `idRegion` int(11) NOT NULL,
  `libelleRegion` varchar(50) NOT NULL,
  `drapeauRegion` varchar(50) DEFAULT NULL,
  `idPays` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Structure de la table `remporter`
--

CREATE TABLE `remporter` (
  `idProfil` int(11) NOT NULL,
  `idTitre` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Déchargement des données de la table `remporter`
--

INSERT INTO `remporter` (`idProfil`, `idTitre`) VALUES
(1, 1),
(1, 2);

-- --------------------------------------------------------

--
-- Structure de la table `repondre`
--

CREATE TABLE `repondre` (
  `idProfil` int(11) NOT NULL,
  `idPartie` int(11) NOT NULL,
  `idQuestion` int(11) NOT NULL,
  `reponse` char(1) NOT NULL,
  `points` tinyint(4) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Déchargement des données de la table `repondre`
--

INSERT INTO `repondre` (`idProfil`, `idPartie`, `idQuestion`, `reponse`, `points`) VALUES
(1, 1, 1, 'b', 0),
(1, 1, 2, 'a', 12),
(1, 1, 3, 'd', 0),
(1, 1, 4, 'a', 10),
(1, 1, 5, 'a', 84),
(1, 1, 6, 'b', 0),
(1, 1, 7, 'a', 12),
(1, 4, 8, 'd', 0),
(1, 4, 9, 'd', 0),
(1, 4, 10, 'a', 12),
(1, 4, 11, 'd', 0),
(1, 4, 12, 'a', 7),
(1, 4, 14, 'c', 0),
(1, 4, 15, 'd', 0),
(1, 13, 1, 'a', 12),
(1, 13, 2, 'a', 10),
(1, 13, 4, 'a', 9),
(1, 13, 5, 'a', 84),
(1, 13, 6, 'b', 0),
(1, 13, 7, 'a', 12),
(1, 13, 17, 'b', 0),
(1, 15, 1, 'a', 12),
(1, 15, 2, 'a', 12),
(1, 15, 3, 'a', 10),
(1, 15, 4, 'd', 0),
(1, 15, 5, 'a', 12),
(1, 15, 7, 'a', 12),
(1, 15, 17, 'a', 9),
(1, 16, 8, 'b', 0),
(1, 16, 9, 'a', 12),
(1, 16, 10, 'c', 0),
(1, 16, 11, 'b', 0),
(1, 16, 12, 'c', 0),
(1, 16, 14, 'a', 12),
(1, 16, 15, 'b', 0),
(1, 18, 19, 'd', 0),
(1, 18, 20, 'a', 12),
(1, 18, 21, 'd', 0),
(1, 18, 22, 'a', 6),
(1, 18, 23, 'a', 6),
(1, 18, 24, 'a', 60),
(1, 18, 25, 'a', 12),
(1, 19, 19, 'a', 12),
(1, 19, 20, 'a', 12),
(1, 19, 21, 'a', 12),
(1, 19, 22, 'a', 12),
(1, 19, 23, 'a', 12),
(1, 19, 24, 'a', 71),
(1, 19, 25, 'a', 12),
(1, 23, 2, 'a', 84),
(1, 23, 3, 'b', 0),
(1, 23, 4, 'd', 0),
(1, 23, 5, 'a', 12),
(1, 23, 6, 'a', 12),
(1, 23, 7, 'a', 12),
(1, 23, 17, 'a', 12),
(1, 24, 26, 'a', 10),
(1, 24, 29, 'a', 9),
(1, 24, 30, 'a', 10),
(1, 24, 32, 'a', 12),
(1, 24, 33, 'a', 9),
(1, 24, 34, 'a', 12),
(1, 24, 35, 'a', 81),
(1, 25, 26, 'a', 12),
(1, 25, 27, 'a', 12),
(1, 25, 31, 'a', 9),
(1, 25, 32, 'a', 12),
(1, 25, 33, 'a', 12),
(1, 25, 34, 'a', 12),
(1, 25, 35, 'a', 84),
(1, 26, 26, 'a', 12),
(1, 26, 27, 'a', 12),
(1, 26, 28, 'a', 12),
(1, 26, 29, 'a', 12),
(1, 26, 30, 'a', 84),
(1, 26, 32, 'a', 12),
(1, 26, 34, 'a', 12),
(1, 27, 26, 'a', 12),
(1, 27, 28, 'a', 12),
(1, 27, 30, 'a', 84),
(1, 27, 32, 'a', 12),
(1, 27, 33, 'a', 12),
(1, 27, 34, 'a', 12),
(1, 27, 35, 'a', 12),
(1, 28, 19, 'a', 12),
(1, 28, 20, 'a', 12),
(1, 28, 21, 'a', 12),
(1, 28, 22, 'a', 12),
(1, 28, 23, 'c', 0),
(1, 28, 24, 'a', 71),
(1, 28, 25, 'd', 0),
(1, 32, 1, 'd', 0),
(1, 32, 2, 'b', 0),
(1, 32, 3, 'a', 70),
(1, 32, 4, 'c', 0),
(1, 32, 5, 'd', 0),
(1, 32, 7, 'd', 0),
(1, 32, 17, 'b', 0),
(1, 33, 1, 'a', 70),
(1, 33, 2, 'd', 0),
(1, 33, 3, 'b', 0),
(1, 33, 4, 'd', 0),
(1, 33, 6, 'c', 0),
(1, 33, 7, 'b', 0),
(1, 33, 17, 'a', 10),
(1, 34, 2, 'a', 10),
(1, 34, 3, 'a', 10),
(1, 34, 4, 'a', 10),
(1, 34, 5, 'd', 0),
(1, 34, 6, 'd', 0),
(1, 34, 7, 'c', 0),
(1, 34, 17, 'c', 0),
(1, 35, 26, 'b', 0),
(1, 35, 27, 'a', 70),
(1, 35, 30, 'c', 0),
(1, 35, 31, 'a', 10),
(1, 35, 32, 'b', 0),
(1, 35, 33, 'c', 0),
(1, 35, 35, 'b', 0),
(1, 39, 1, 'a', 10),
(1, 39, 2, 'b', 0),
(1, 39, 3, 'a', 63),
(1, 39, 4, 'a', 10),
(1, 39, 5, 'b', 0),
(1, 39, 6, 'c', 0),
(1, 39, 7, 'c', 0),
(2, 2, 1, 'a', 42),
(2, 2, 2, 'a', 8),
(2, 2, 3, 'a', 9),
(2, 2, 4, 'a', 9),
(2, 2, 5, 'a', 9),
(2, 2, 6, 'a', 8),
(2, 2, 7, 'a', 7),
(2, 3, 1, 'a', 8),
(2, 3, 2, 'a', 9),
(2, 3, 3, 'a', 0),
(2, 3, 4, 'a', 9),
(2, 3, 5, 'a', 7),
(2, 3, 6, 'a', 7),
(2, 3, 7, 'a', 21),
(2, 5, 8, 'a', 9),
(2, 5, 9, 'a', 9),
(2, 5, 10, 'a', 56),
(2, 5, 11, 'b', 0),
(2, 5, 12, 'a', 10),
(2, 5, 14, 'a', 9),
(2, 5, 15, 'a', 8),
(6, 6, 1, 'a', 4),
(6, 6, 2, 'd', 0),
(6, 6, 3, 'a', 4),
(6, 6, 4, 'b', 0),
(6, 6, 5, 'a', 6),
(6, 6, 6, 'a', 3),
(6, 6, 7, 'a', 28),
(8, 14, 1, 'b', 0),
(8, 14, 2, 'a', 6),
(8, 14, 3, 'a', 7),
(8, 14, 4, 'a', 3),
(8, 14, 5, 'a', 4),
(8, 14, 6, 'c', 0),
(8, 14, 17, 'c', 0),
(8, 20, 19, 'a', 6),
(8, 20, 20, 'b', 0),
(8, 20, 21, 'a', 5),
(8, 20, 22, 'a', 5),
(8, 20, 23, 'b', 0),
(8, 20, 24, 'a', 3),
(8, 20, 25, 'a', 35),
(9, 7, 1, 'a', 7),
(9, 7, 2, 'a', 10),
(9, 7, 3, 'a', 8),
(9, 7, 4, 'a', 7),
(9, 7, 5, 'a', 9),
(9, 7, 6, 'a', 42),
(9, 7, 7, 'a', 7),
(9, 8, 1, 'a', 63),
(9, 8, 2, 'a', 10),
(9, 8, 3, 'a', 10),
(9, 8, 4, 'a', 9),
(9, 8, 5, 'a', 9),
(9, 8, 6, 'a', 9),
(9, 8, 7, 'a', 9),
(9, 9, 1, 'a', 10),
(9, 9, 2, 'a', 10),
(9, 9, 3, 'a', 9),
(9, 9, 4, 'a', 8),
(9, 9, 5, 'a', 9),
(9, 9, 7, 'a', 10),
(9, 9, 17, 'a', 70),
(10, 10, 2, 'd', 0),
(10, 10, 3, 'a', 7),
(10, 10, 4, 'b', 0),
(10, 10, 5, 'a', 42),
(10, 10, 6, 'a', 6),
(10, 10, 7, 'b', 0),
(10, 10, 17, 'a', 4),
(10, 11, 8, 'b', 0),
(10, 11, 9, 'b', 0),
(10, 11, 10, 'a', 7),
(10, 11, 11, 'c', 0),
(10, 11, 12, 'b', 0),
(10, 11, 14, 'a', 7),
(10, 11, 15, 'd', 0),
(10, 12, 1, 'b', 0),
(10, 12, 3, 'a', 10),
(10, 12, 4, 'a', 9),
(10, 12, 5, 'a', 9),
(10, 12, 6, 'a', 9),
(10, 12, 7, 'a', 70),
(10, 12, 17, 'a', 8),
(11, 17, 1, 'b', 0),
(11, 17, 3, 'a', 10),
(11, 17, 4, 'a', 4),
(11, 17, 5, 'a', 4),
(11, 17, 6, 'b', 0),
(11, 17, 7, 'b', 0),
(11, 17, 17, 'a', 28),
(12, 21, 19, 'a', 3),
(12, 21, 20, 'a', 6),
(12, 21, 21, 'a', 3),
(12, 21, 22, 'a', 6),
(12, 21, 23, 'c', 0),
(12, 21, 24, 'a', 3),
(12, 21, 25, 'd', 0),
(12, 22, 1, 'b', 0),
(12, 22, 2, 'a', 5),
(12, 22, 3, 'a', 8),
(12, 22, 4, 'd', 0),
(12, 22, 5, 'a', 7),
(12, 22, 7, 'a', 5),
(12, 22, 17, 'b', 0),
(15, 29, 1, 'a', 8),
(15, 29, 3, 'a', 6),
(15, 29, 4, 'c', 0),
(15, 29, 5, 'a', 4),
(15, 29, 6, 'b', 0),
(15, 29, 7, 'a', 6),
(15, 29, 17, 'a', 6),
(16, 30, 28, 'a', 4),
(16, 30, 29, 'b', 0),
(16, 30, 30, 'd', 0),
(16, 30, 31, 'a', 7),
(16, 30, 32, 'a', 6),
(16, 30, 33, 'd', 0),
(16, 30, 35, 'a', 0),
(16, 31, 26, 'a', 6),
(16, 31, 27, 'a', 7),
(16, 31, 30, 'a', 7),
(16, 31, 31, 'a', 7),
(16, 31, 32, 'a', 5),
(16, 31, 33, 'a', 28),
(16, 31, 35, 'a', 7),
(17, 36, 19, 'a', 3),
(17, 36, 20, 'd', 0),
(17, 36, 21, 'c', 0),
(17, 36, 22, 'a', 28),
(17, 36, 23, 'c', 0),
(17, 36, 24, 'c', 0),
(17, 36, 25, 'a', 6),
(17, 37, 8, 'a', 5),
(17, 37, 9, 'd', 0),
(17, 37, 10, 'a', 7),
(17, 37, 11, 'c', 0),
(17, 37, 12, 'a', 7),
(17, 37, 14, 'c', 0),
(17, 37, 15, 'a', 5),
(17, 38, 19, 'a', 9),
(17, 38, 20, 'a', 9),
(17, 38, 21, 'a', 7),
(17, 38, 22, 'a', 9),
(17, 38, 23, 'a', 8),
(17, 38, 24, 'a', 7),
(17, 38, 25, 'a', 63);

-- --------------------------------------------------------

--
-- Structure de la table `suivre`
--

CREATE TABLE `suivre` (
  `idProfil` int(11) NOT NULL,
  `idTheme` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Déchargement des données de la table `suivre`
--

INSERT INTO `suivre` (`idProfil`, `idTheme`) VALUES
(1, 1),
(1, 2),
(1, 5),
(1, 6),
(13, 6);

-- --------------------------------------------------------

--
-- Structure de la table `s_abonner`
--

CREATE TABLE `s_abonner` (
  `idProfil` int(11) NOT NULL,
  `idProfil_1` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Déchargement des données de la table `s_abonner`
--

INSERT INTO `s_abonner` (`idProfil`, `idProfil_1`) VALUES
(1, 2),
(1, 3),
(8, 1);

-- --------------------------------------------------------

--
-- Structure de la table `theme`
--

CREATE TABLE `theme` (
  `idTheme` int(11) NOT NULL,
  `libelleTheme` varchar(50) NOT NULL,
  `description` varchar(250) NOT NULL,
  `logo` varchar(50) NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `idCategorie` int(11) NOT NULL,
  `idProfil` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Déchargement des données de la table `theme`
--

INSERT INTO `theme` (`idTheme`, `libelleTheme`, `description`, `logo`, `dateUpdated`, `idCategorie`, `idProfil`) VALUES
(1, 'Chimie', 'Êtes-vous fort en chimie ? Allez voir ça !', '/img/themes/5cdda9e1e1188.png', '2019-05-16 22:24:40', 1, 1),
(2, 'Jeux Olympiques', 'Connaissez-vous vraiment les jeux olympiques ?', '/img/themes/5cddb370a0a39.jpg', '2019-05-16 19:33:35', 2, 2),
(3, 'The Walking Dead', 'Conaissez vous la série par coeur !? ', '/img/themes/5cddba686c904.jpg', '2019-05-16 19:33:35', 3, 1),
(4, 'Game Of Thrones', 'Conaissez vous la série par coeur !? ', '/img/themes/5cddbafe45724.jpeg', '2019-05-17 06:30:15', 3, 1),
(5, 'Programmation', 'Tout ce qui touche à la programmation: langages, concepts, architecture, etc.', '/img/themes/5cde900ecf175.jpg', '2019-05-17 16:32:33', 4, 8),
(6, 'Columbo', 'Pour les fans de cette série mythique', '/img/themes/5ce16f45c9ae5.jpg', '2019-05-19 15:10:27', 3, 1);

-- --------------------------------------------------------

--
-- Structure de la table `titre`
--

CREATE TABLE `titre` (
  `idTitre` int(11) NOT NULL,
  `libelleTitre` varchar(250) NOT NULL,
  `niveauRequis` tinyint(4) NOT NULL,
  `idTheme` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Déchargement des données de la table `titre`
--

INSERT INTO `titre` (`idTitre`, `libelleTitre`, `niveauRequis`, `idTheme`) VALUES
(1, 'Débutant chimique !', 2, 1),
(2, 'Petit débutant', 2, 6),
(3, 'Petit sportif !', 2, 2);

-- --------------------------------------------------------

--
-- Structure de la table `usersession`
--

CREATE TABLE `usersession` (
  `email` varchar(50) NOT NULL,
  `password` varchar(255) NOT NULL,
  `lastActivity` datetime NOT NULL,
  `joursConsecutifs` tinyint(4) NOT NULL,
  `idProfil` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Déchargement des données de la table `usersession`
--

INSERT INTO `usersession` (`email`, `password`, `lastActivity`, `joursConsecutifs`, `idProfil`) VALUES
('babouchedu42@gmail.com', '7508dde48bb5e43cf0e4e5d183e5b97dc9f337f5d5b36c6f8caf2a8d77a54fb3', '2019-05-18 16:01:34', 0, 14),
('djschtroumpf@live.be', '9838980c35f9c8a7b9b1bd154b55295cab9b9a28f363763a5413e3ee8926abe0', '2019-05-16 19:59:04', 1, 6),
('Ee@ee.com', '88d516e73f9988cfd8f9b3ebb34e8b5d018a709162b47120fc2b5fcfb9abb34e', '2019-05-16 19:17:14', 0, 5),
('el_therese@voo.be', 'a0a67a7b73e47b2ffc82b04f0601fd7845a02dfd57c8af18d91dc5e4a5a086fb', '2019-05-19 17:27:23', 0, 16),
('florent-banneux@hotmail.com', '0fd08cac90e89dcf7b993c38161c5197d827269801e0da14d9f7ed4d26337314', '2019-05-16 18:41:45', 0, 4),
('jo.toussaint@student.helmo.be', 'f62ad3449bad38735153536f12913785c10e4ee2a6de322bd52b860fc4f87c9f', '2019-05-20 16:16:13', 0, 17),
('Kouristoll@hotmail.be', '00409cafc6f67a4a0da6ec4d4a8eb42b213e1f5de90229f58e7069f8e97b9a12', '2019-05-19 16:08:40', 0, 15),
('legrandmuraille@hotmail.be', '960cf597a64da6cb5b22c66ac6db90a1b2b029406e89111710702e4191dd83c1', '2019-05-24 09:15:46', 1, 11),
('ligotcindy@gmail.com', '56cb4f6f66fc225df2ee26af816e9bd5a6f2ab89f5eaecf10dd0a91b5e1cad69', '2019-05-16 20:02:24', 0, 7),
('loic.collette@gmail.com', '7c6467a65cdf95814c2c17aa7c82f4bfe1c22668aa3db5e8866fe13f97ac2432', '2019-06-14 21:23:34', 0, 1),
('loic.glineur@hotmail.be', '7e9018a42f7f67aef4786fa3c95932946cfe5fb2ab7b288ebbe38942ff6f5753', '2019-05-16 18:48:49', 0, 2),
('martin.blavier@hotmail.fr', '7544044dc51464e4eb6bccf8001bc9dcf05f693ce4ed1cf4bc89a8427cb36319', '2019-05-16 22:20:43', 0, 9),
('nisealo@sjb-liege.org', '3f21a8490cef2bfb60a9702e9d2ddb7a805c9bd1a263557dfd51a7d0e9dfa93e', '2019-05-18 08:58:10', 0, 12),
('petitloic@gmail.com', 'c140af061d0cc639396b7eb1c392101d688d0850a4fb37589cef652c98602153', '2019-05-23 21:33:18', 0, 18),
('plouf@sagot.be', '0a051d43846e6308bf97e890804494c63ca2e6a669945531bae177b441b04cdc', '2019-04-30 08:45:24', 1, 3),
('romain10weynand@gmail.com', 'eceae3c7ec1bb978e793d371de4f44acb5116c8947d18f93606aa5c0f87416e7', '2019-05-16 22:29:52', 0, 10),
('test@live.be', '054bc835a4b4bec9e3dc2faa8015c7df0b46e12be5411d4b5f5e38f2db3b8ec5', '2019-05-18 14:40:33', 0, 13),
('xeyibulege@mail-finder.net', '63a9636f785515295efeb618c69b4cc4184bc04e1201a45d38ff146cd987663a', '2019-05-17 17:39:50', 1, 8);

--
-- Index pour les tables déchargées
--

--
-- Index pour la table `bloquer`
--
ALTER TABLE `bloquer`
  ADD PRIMARY KEY (`idProfil`,`idProfil_1`),
  ADD KEY `bloquer_ibfk_2` (`idProfil_1`);

--
-- Index pour la table `categorie`
--
ALTER TABLE `categorie`
  ADD PRIMARY KEY (`idCategorie`);

--
-- Index pour la table `chat_msg`
--
ALTER TABLE `chat_msg`
  ADD PRIMARY KEY (`idChatMsg`),
  ADD KEY `chat_msg_ibfk_1` (`idProfil_recepteur`),
  ADD KEY `chat_msg_ibfk_2` (`idProfil_emetteur`);

--
-- Index pour la table `integrer`
--
ALTER TABLE `integrer`
  ADD PRIMARY KEY (`idPartie`,`idQuestion`),
  ADD KEY `idQuestion` (`idQuestion`);

--
-- Index pour la table `liker`
--
ALTER TABLE `liker`
  ADD PRIMARY KEY (`idProfil`,`idMessage`),
  ADD KEY `idMessage` (`idMessage`);

--
-- Index pour la table `message`
--
ALTER TABLE `message`
  ADD PRIMARY KEY (`idMessage`),
  ADD KEY `idMessage_1` (`idMessage_1`),
  ADD KEY `idTheme` (`idTheme`),
  ADD KEY `message_ibfk_3` (`idProfil`);

--
-- Index pour la table `participer`
--
ALTER TABLE `participer`
  ADD PRIMARY KEY (`idProfil`,`idPartie`),
  ADD KEY `idPartie` (`idPartie`);

--
-- Index pour la table `partie`
--
ALTER TABLE `partie`
  ADD PRIMARY KEY (`idPartie`);

--
-- Index pour la table `pays`
--
ALTER TABLE `pays`
  ADD PRIMARY KEY (`idPays`),
  ADD UNIQUE KEY `libellePays` (`libellePays`);

--
-- Index pour la table `profil`
--
ALTER TABLE `profil`
  ADD PRIMARY KEY (`idProfil`),
  ADD KEY `idPays` (`idPays`),
  ADD KEY `idRegion` (`idRegion`),
  ADD KEY `idTitre` (`idTitre`);

--
-- Index pour la table `question`
--
ALTER TABLE `question`
  ADD PRIMARY KEY (`idQuestion`),
  ADD KEY `idTheme` (`idTheme`);

--
-- Index pour la table `region`
--
ALTER TABLE `region`
  ADD PRIMARY KEY (`idRegion`),
  ADD UNIQUE KEY `libelleRegion` (`libelleRegion`),
  ADD KEY `idPays` (`idPays`);

--
-- Index pour la table `remporter`
--
ALTER TABLE `remporter`
  ADD PRIMARY KEY (`idProfil`,`idTitre`),
  ADD KEY `idTitre` (`idTitre`);

--
-- Index pour la table `repondre`
--
ALTER TABLE `repondre`
  ADD PRIMARY KEY (`idProfil`,`idPartie`,`idQuestion`),
  ADD KEY `idPartie` (`idPartie`),
  ADD KEY `idQuestion` (`idQuestion`);

--
-- Index pour la table `suivre`
--
ALTER TABLE `suivre`
  ADD PRIMARY KEY (`idProfil`,`idTheme`),
  ADD KEY `suivre_ibfk_2` (`idTheme`);

--
-- Index pour la table `s_abonner`
--
ALTER TABLE `s_abonner`
  ADD PRIMARY KEY (`idProfil`,`idProfil_1`),
  ADD KEY `s_abonner_ibfk_2` (`idProfil_1`);

--
-- Index pour la table `theme`
--
ALTER TABLE `theme`
  ADD PRIMARY KEY (`idTheme`),
  ADD KEY `idCategorie` (`idCategorie`),
  ADD KEY `idProfil` (`idProfil`);

--
-- Index pour la table `titre`
--
ALTER TABLE `titre`
  ADD PRIMARY KEY (`idTitre`),
  ADD KEY `idTheme` (`idTheme`);

--
-- Index pour la table `usersession`
--
ALTER TABLE `usersession`
  ADD PRIMARY KEY (`email`),
  ADD KEY `idProfil` (`idProfil`);

--
-- AUTO_INCREMENT pour les tables déchargées
--

--
-- AUTO_INCREMENT pour la table `categorie`
--
ALTER TABLE `categorie`
  MODIFY `idCategorie` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT pour la table `chat_msg`
--
ALTER TABLE `chat_msg`
  MODIFY `idChatMsg` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;

--
-- AUTO_INCREMENT pour la table `message`
--
ALTER TABLE `message`
  MODIFY `idMessage` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT pour la table `partie`
--
ALTER TABLE `partie`
  MODIFY `idPartie` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=40;

--
-- AUTO_INCREMENT pour la table `pays`
--
ALTER TABLE `pays`
  MODIFY `idPays` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT pour la table `profil`
--
ALTER TABLE `profil`
  MODIFY `idProfil` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;

--
-- AUTO_INCREMENT pour la table `question`
--
ALTER TABLE `question`
  MODIFY `idQuestion` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=36;

--
-- AUTO_INCREMENT pour la table `region`
--
ALTER TABLE `region`
  MODIFY `idRegion` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT pour la table `theme`
--
ALTER TABLE `theme`
  MODIFY `idTheme` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT pour la table `titre`
--
ALTER TABLE `titre`
  MODIFY `idTitre` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- Contraintes pour les tables déchargées
--

--
-- Contraintes pour la table `bloquer`
--
ALTER TABLE `bloquer`
  ADD CONSTRAINT `bloquer_ibfk_1` FOREIGN KEY (`idProfil`) REFERENCES `profil` (`idProfil`),
  ADD CONSTRAINT `bloquer_ibfk_2` FOREIGN KEY (`idProfil_1`) REFERENCES `profil` (`idProfil`);

--
-- Contraintes pour la table `chat_msg`
--
ALTER TABLE `chat_msg`
  ADD CONSTRAINT `chat_msg_ibfk_1` FOREIGN KEY (`idProfil_recepteur`) REFERENCES `profil` (`idProfil`),
  ADD CONSTRAINT `chat_msg_ibfk_2` FOREIGN KEY (`idProfil_emetteur`) REFERENCES `profil` (`idProfil`);

--
-- Contraintes pour la table `integrer`
--
ALTER TABLE `integrer`
  ADD CONSTRAINT `integrer_ibfk_1` FOREIGN KEY (`idPartie`) REFERENCES `partie` (`idPartie`),
  ADD CONSTRAINT `integrer_ibfk_2` FOREIGN KEY (`idQuestion`) REFERENCES `question` (`idQuestion`);

--
-- Contraintes pour la table `liker`
--
ALTER TABLE `liker`
  ADD CONSTRAINT `liker_ibfk_1` FOREIGN KEY (`idProfil`) REFERENCES `profil` (`idProfil`),
  ADD CONSTRAINT `liker_ibfk_2` FOREIGN KEY (`idMessage`) REFERENCES `message` (`idMessage`);

--
-- Contraintes pour la table `message`
--
ALTER TABLE `message`
  ADD CONSTRAINT `message_ibfk_1` FOREIGN KEY (`idMessage_1`) REFERENCES `message` (`idMessage`),
  ADD CONSTRAINT `message_ibfk_2` FOREIGN KEY (`idTheme`) REFERENCES `theme` (`idTheme`),
  ADD CONSTRAINT `message_ibfk_3` FOREIGN KEY (`idProfil`) REFERENCES `profil` (`idProfil`);

--
-- Contraintes pour la table `participer`
--
ALTER TABLE `participer`
  ADD CONSTRAINT `participer_ibfk_1` FOREIGN KEY (`idProfil`) REFERENCES `profil` (`idProfil`),
  ADD CONSTRAINT `participer_ibfk_2` FOREIGN KEY (`idPartie`) REFERENCES `partie` (`idPartie`);

--
-- Contraintes pour la table `profil`
--
ALTER TABLE `profil`
  ADD CONSTRAINT `profil_ibfk_1` FOREIGN KEY (`idPays`) REFERENCES `pays` (`idPays`),
  ADD CONSTRAINT `profil_ibfk_2` FOREIGN KEY (`idRegion`) REFERENCES `region` (`idRegion`),
  ADD CONSTRAINT `profil_ibfk_3` FOREIGN KEY (`idTitre`) REFERENCES `titre` (`idTitre`);

--
-- Contraintes pour la table `question`
--
ALTER TABLE `question`
  ADD CONSTRAINT `question_ibfk_1` FOREIGN KEY (`idTheme`) REFERENCES `theme` (`idTheme`);

--
-- Contraintes pour la table `region`
--
ALTER TABLE `region`
  ADD CONSTRAINT `region_ibfk_1` FOREIGN KEY (`idPays`) REFERENCES `pays` (`idPays`);

--
-- Contraintes pour la table `remporter`
--
ALTER TABLE `remporter`
  ADD CONSTRAINT `remporter_ibfk_1` FOREIGN KEY (`idProfil`) REFERENCES `profil` (`idProfil`),
  ADD CONSTRAINT `remporter_ibfk_2` FOREIGN KEY (`idTitre`) REFERENCES `titre` (`idTitre`);

--
-- Contraintes pour la table `repondre`
--
ALTER TABLE `repondre`
  ADD CONSTRAINT `repondre_ibfk_1` FOREIGN KEY (`idProfil`) REFERENCES `profil` (`idProfil`),
  ADD CONSTRAINT `repondre_ibfk_2` FOREIGN KEY (`idPartie`) REFERENCES `partie` (`idPartie`),
  ADD CONSTRAINT `repondre_ibfk_3` FOREIGN KEY (`idQuestion`) REFERENCES `question` (`idQuestion`);

--
-- Contraintes pour la table `suivre`
--
ALTER TABLE `suivre`
  ADD CONSTRAINT `suivre_ibfk_1` FOREIGN KEY (`idProfil`) REFERENCES `profil` (`idProfil`),
  ADD CONSTRAINT `suivre_ibfk_2` FOREIGN KEY (`idTheme`) REFERENCES `theme` (`idTheme`);

--
-- Contraintes pour la table `s_abonner`
--
ALTER TABLE `s_abonner`
  ADD CONSTRAINT `s_abonner_ibfk_1` FOREIGN KEY (`idProfil`) REFERENCES `profil` (`idProfil`),
  ADD CONSTRAINT `s_abonner_ibfk_2` FOREIGN KEY (`idProfil_1`) REFERENCES `profil` (`idProfil`);

--
-- Contraintes pour la table `theme`
--
ALTER TABLE `theme`
  ADD CONSTRAINT `theme_ibfk_1` FOREIGN KEY (`idCategorie`) REFERENCES `categorie` (`idCategorie`),
  ADD CONSTRAINT `theme_ibfk_2` FOREIGN KEY (`idProfil`) REFERENCES `profil` (`idProfil`);

--
-- Contraintes pour la table `titre`
--
ALTER TABLE `titre`
  ADD CONSTRAINT `titre_ibfk_1` FOREIGN KEY (`idTheme`) REFERENCES `theme` (`idTheme`);

--
-- Contraintes pour la table `usersession`
--
ALTER TABLE `usersession`
  ADD CONSTRAINT `usersession_ibfk_1` FOREIGN KEY (`idProfil`) REFERENCES `profil` (`idProfil`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
