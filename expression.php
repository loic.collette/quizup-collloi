<?php
/**
 * Created by PhpStorm.
 * User: collloi
 * Date: 12/03/19
 * Time: 8:26
 */

?>

<!DOCTYPE html>
<html>
<head>
    <title>Expressions regulières</title>
</head>
<body>


<h1>Expressions régulières</h1>
<hr/>
<h2>Vérifier une adresse email</h2>
    <?php

    $emailOk = "coucou@gmail.com";
    $emailKo = "coucougmail.c";
    $pattern = "#^[a-z0-9._-]+@[a-z0-9._-]{2,}\.[a-z]{2,4}$#";





    ?>

<p>
    Email : <?php echo($emailOk); ?> => <?php echo(var_dump(preg_match($pattern, $emailOk))); ?>
</p>
<p>
    Email : <?php echo($emailKo); ?> => <?php echo(var_dump(preg_match($pattern, $emailKo))); ?>
</p>

<hr/>
<h2>Trouver un mot</h2>
<?php

$motOk = "laboratoire";
$motKo = "labo";
$motOki = "LABORatoire";

$match = "#laboratoire#";
$matchi = "#laboratoire#i";



?>
<p>
    Trouver "laboratoire" : <?php echo($match); ?>

</p>
<p>
    Mot $motOk = <?php echo($motOk); ?><br/>
    Match = <?php var_dump(preg_match($match, $motOk)); ?><br/>
    Matchi = <?php var_dump(preg_match($matchi, $motOk)); ?>
</p>

<p>
    Mot $motKo = <?php echo($motKo); ?><br/>
    Match = <?php var_dump(preg_match($match, $motKo)); ?><br/>
    Matchi = <?php var_dump(preg_match($matchi, $motKo)); ?>
</p>

<p>
    Mot $motOki = <?php echo($motOki); ?><br/>
    Match = <?php var_dump(preg_match($match, $motOki)); ?><br/>
    Matchi = <?php var_dump(preg_match($matchi, $motOki)); ?>
</p>

<hr/>
<h2>Exos</h2>
<p>
    Code postal : 4032 => <?php
    var_dump(preg_match("#^[1-9][0-9]{3}$#", "4032"));
    ?>
</p>
<p>
    Titre html h1 - h6 : => <?php
    var_dump(preg_match("#</?h[1-6]>#", "</h1>"));
    ?>
</p>
<p>
    Titre TVA Belge/Francais : => <?php
    var_dump(preg_match("#^FR|BE[0]([0-9]{3}\.){2}[0-9]{3}$#", "BE0123.112.103"));
    ?>
</p>

<p>
    COmpte belge : => <?php
    var_dump(preg_match("#^[0-9]{3}-[0-9]{7}-[0-9]{2}$#", "123-1231231-25"));
    ?>
</p>
<p>
    Gris/gros/gras: => <?php
    var_dump(preg_match("#gr[i|o|a]s#", "gris"));
    ?>
</p>


</body>

<style>
    h2 {
        margin-left: 2%;
    }

    p {
        margin-left: 3%;
    }
</style>

</html>