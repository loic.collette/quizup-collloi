<?php
/**
 * Created by PhpStorm.
 * User: collloi
 * Date: 12/03/19
 * Time: 12:56
 */
require_once(__DIR__ . "/../php/database/connect.php");
require_once(__DIR__ . "/../php/functions/themes.php");
require_once(__DIR__ . "/../php/functions/user.php");
require_once(__DIR__ . "/../php/functions/social.php");

redirectIfnotLoggedIn();


?>

<html>
<head>
    <title>Accueil</title>
    <?php require_once(__DIR__ . "/../inc/head.php"); ?>
</head>
<body>

<?php require_once(__DIR__ . "/../inc/nav.php"); ?>

<?php if (isset($_GET["recherche"]) && strlen($_GET["recherche"]) <= 3){ ?>
<section>
    <header>
        <h2>Attention </h2>
    </header>

    <article>

        <div class="alert alert-warning">
            <strong>Attention !</strong> Recherche trop courte !
        </div>

    </article>
</section>
<?php } else { ?>


<section>
    <header>
        <h2>Recherche d'utilisateurs </h2>
    </header>

    <article>


        <?php if (!isset($_GET["recherche"]) || empty($_GET["recherche"])) { ?>
            <div class="alert alert-warning">
                <strong>Attention !</strong> Recherche vide !
            </div>
        <?php } else {
            $recherche = addslashes(htmlspecialchars($_GET["recherche"]));

            $result = mysqli_query($bdd, "SELECT * FROM profil WHERE nomProfil like '%" . $recherche . "%'");

            if ($result) {
                ?>

                <table class="table table-hover">
                    <thead>

                    <tr>
                        <th style="width: 256px;">
                            Photo de profil
                        </th>
                        <th>
                            Nom d'utilisateur
                        </th>
                        <th>
                            Voir
                        </th>
                    </tr>

                    </thead>
                    <tbody>

                    <?php while ($row = mysqli_fetch_array($result)) { ?>

                        <tr>
                            <td>
                                <img>
                            </td>
                            <td>
                                <?php echo($row["nomProfil"]); ?>
                            </td>
                            <td>
                                <a href="./profile.php?idProfil=<?php echo($row["idProfil"]); ?>"
                                   class="btn btn-success btn-lg">Voir</a>
                            </td>
                        </tr>

                    <?php } ?>

                    </tbody>
                </table>
            <?php } else {
                ?>

                <div class="alert alert-warning">
                    <strong>Attention !</strong> La recherche n'a retourné aucun résultat.
                </div>


                <?php
            }
        } ?>

    </article>
</section>

<section>
    <header>
        <h2>Thèmes </h2>
    </header>

    <article>


        <?php
        if (!empty($_GET["recherche"])) {
            $result = mysqli_query($bdd, "SELECT * FROM theme WHERE libelleTheme like '%" . $recherche . "%'");

            if ($result) {
                ?>

                <table class="table table-hover">
                    <thead>

                    <tr>
                        <th style="width: 256px;">
                            Icone
                        </th>
                        <th>
                            Nom du thème
                        </th>
                        <th>
                            Voir
                        </th>
                    </tr>

                    </thead>
                    <tbody>

                    <?php while ($row = mysqli_fetch_array($result)) { ?>

                        <tr>
                            <td>
                                <img>
                            </td>
                            <td>
                                <?php echo($row["libelleTheme"]); ?>
                            </td>
                            <td>
                                <a href="/game/voir.php?idTheme=<?php echo($row["idTheme"]); ?>"
                                   class="btn btn-success btn-lg">Voir</a>
                            </td>
                        </tr>

                    <?php } ?>

                    </tbody>
                </table>
            <?php } else {
                ?>

                <div class="alert alert-warning">
                    <strong>Attention !</strong> La recherche n'a retourné aucun résultat.
                </div>


                <?php

            }
        } else {
            ?>


            <div class="alert alert-warning">
                <strong>Attention !</strong> Recherche vide !
            </div>


            <?php

        } ?>

    </article>
</section>

<?php } ?>

<?php require_once(__DIR__ . "/../inc/footer.php"); ?>

</body>
</html>

