<?php
/**
 * Created by PhpStorm.
 * User: collloi
 * Date: 18/03/19
 * Time: 12:26
 */

@session_start();
require_once(__DIR__ . "/../php/functions/user.php");
require_once(__DIR__ . "/../php/functions/social.php");

redirectIfnotLoggedIn();

if (isset($_GET["idProfil"])) {
    $idProfile = addslashes(htmlspecialchars($_GET["idProfil"]));
} else if (isLoggedIn()) {
    $idProfile = addslashes(htmlspecialchars($_SESSION["idProfil"]));
} else {
    header("Location: /");
    die();
}

$details = getProfileDetails($idProfile);

if (!$details["success"]) {

    header("Location: /");
    die();
}


?>

<!--


array(6) {
    ["success"]=> bool(true)
    ["details"]=> array(9) {
        ["private"]=> string(1) "0"
        ["username"]=> string(8) "loic2665"
        ["photo"]=> string(31) "/img/users/profile/loic2665.png"
        ["facade"]=> string(30) "/img/users/facade/loic2665.png"
        ["ville"]=> string(6) "Liège"
        ["bio"]=> NULL
        ["titre"]=> string(42) "Erreur lors de la récupération du titre."
        ["pays"]=> string(8) "Belgique"
        ["region"]=> string(47) "Erreur lors de la récupération de la région."
    }

    ["blockedByUser"]=> bool(false)
    ["isFollowingHim"]=> bool(false)
    ["isHeFollowHim"]=> bool(false)
    ["isPersonnalProfile"]=> bool(true)
}


-->

<?php //var_dump($details); ?>


<html>
<head>
    <title>Profil de <?php echo($details["details"]["username"]); ?></title>
    <?php require(__DIR__ . "/../inc/head.php"); ?>
</head>
<body>

<?php require(__DIR__ . "/../inc/nav.php"); ?>


<section>
    <div id="statusServer">

    </div>
    <div class="jumbotron jumboTranp"
         style=" <?php if($details["details"]["facade"]){ ?>background: url(<?php echo($details["details"]["facade"]); } ?>); background-size: cover;">

    </div>

    <div class="jumbotron jumbotron2" style="<?php if(!$details["details"]["facade"]){ ?> background: linear-gradient(0.30turn, #eb6460, #ffffff); <?php }else{ ?>background: rgba(0,0,0,0) <?php } ?>">
        <div class="profilePic" style="background-image: url(<?php echo($details["details"]["photo"]); ?>)"></div>
        <h1 class="display-3 txtWhite">Profil de <?php echo($details["details"]["username"]); ?></h1>
        <p class="lead txtWhite"><?php echo($details["details"]["titre"]); ?></p>
        <hr class="my-4">
        <p class="txtWhite"><?php echo($details["details"]["bio"]); ?></p>
        <p class="lead">
            <?php if (isLoggedIn()) { ?>

                <?php if ($details["isHeFollowHim"] and !$details["blockedByUser"]) { ?>
                <a class="btn btn-primary btn-lg unfollow" href="#" id="unfollow" role="button">Ne plus suivre</a>
                <script>
                    $(".unfollow").click(function () {
                        $.post("./ajax/unfollow.php",
                            {
                                idUser: <?php echo($idProfile); ?>,
                            },

                            function (data, status) {

                                data = JSON.parse(data);

                                if (data["success"] === true) {
                                    sleep(2000).then(() => {
                                        // Do something after the sleep!
                                        window.location.replace("./profile.php?idProfil=" + <?php echo($idProfile); ?>);
                                    });

                                }
                                document.getElementById("statusServer").innerHTML = data["message"];


                            }
                        );
                    });
                </script>


            <?php } else { ?>
                <a class="btn btn-primary btn-lg follow" href="#" id="follow" role="button">Suivre</a>
                <script>
                    $(".follow").click(function () {
                        $.post("./ajax/follow.php",
                            {
                                idUser: <?php echo($idProfile); ?>,
                            },

                            function (data, status) {

                                data = JSON.parse(data);

                                if (data["success"] === true) {
                                    sleep(2000).then(() => {
                                        // Do something after the sleep!
                                        window.location.replace("./profile.php?idProfil=" + <?php echo($idProfile); ?>);
                                    });

                                }
                                document.getElementById("statusServer").innerHTML = data["message"];


                            }
                        );
                    });
                </script>
            <?php } ?>


            <?php if ($details["isHeBlockingHim"]) { ?>
                <a class="btn btn-primary btn-lg unblock" href="#" id="unblock" role="button">Ne plus bloquer</a>
                <script>
                    $(".unblock").click(function () {
                        $.post("./ajax/unblock.php",
                            {
                                idUser: <?php echo($idProfile); ?>,
                            },

                            function (data, status) {

                                data = JSON.parse(data);

                                if (data["success"] === true) {

                                    sleep(2000).then(() => {
                                        // Do something after the sleep!
                                        window.location.replace("./profile.php?idProfil=" + <?php echo($idProfile); ?>);
                                    });

                                }
                                document.getElementById("statusServer").innerHTML = data["message"];


                            }
                        );
                    });
                </script>
            <?php } else { ?>
                <a class="btn btn-primary btn-lg block" href="#" id="block" role="button">Bloquer</a>
                <script>
                    $(".block").click(function () {
                        $.post("./ajax/block.php",
                            {
                                idUser: <?php echo($idProfile); ?>,
                            },

                            function (data, status) {

                                data = JSON.parse(data);

                                if (data["success"] === true) {
                                    sleep(2000).then(() => {
                                        // Do something after the sleep!
                                        window.location.replace("./profile.php?idProfil=" + <?php echo($idProfile); ?>);
                                    });

                                }
                                document.getElementById("statusServer").innerHTML = data["message"];


                            }
                        );
                    });
                </script>
            <?php } ?>

            <?php if (isPersonnalProfile($idProfile)) { ?>
                <a class="btn btn-primary btn-lg" href="/settings.php" role="button">Paramètres</a>
            <?php } ?>

                <a class="btn btn-primary btn-lg" href="/game/listGame.php?idProfil=<?php echo($idProfile); ?>" role="button">Voir les parties jouées</a>
            <?php } else { ?>
                <a class="btn btn-primary btn-lg" href="/login.php" role="button">Se connecter</a>
            <?php } ?>
        </p>
    </div>


</section>

<?php if ($details["blockedByUser"]) { ?>

    <section>

        <header>
            <h2>
                Cet utilisateur t'as bloqué.
            </h2>
        </header>

        <article>


            <div class="alert alert-primary"><strong>Oops... </strong> On m'annonce à l'oreillette que l'utilisateur
                t'as bloqué, tu ne peux donc pas intéragir avec.
            </div>


        </article>

    </section>

<?php } else { ?>

    <section>

        <header>
            <h2>
                Fil d'actualité de <?php echo($details["details"]["username"]); ?>
            </h2>
        </header>

        <article>

            <?php


            $result = mysqli_query($bdd, "SELECT * FROM message LEFT JOIN profil p on message.idProfil = p.idProfil LEFT JOIN theme ON message.idTheme = theme.idTheme WHERE message.idProfil = " . $idProfile . " AND idMessage_1 IS NULL ORDER BY idMessage DESC;");

            if (!$result) {
                ?>

                <div class="alert alert-warning"><strong>Oops... </strong> Aucun post sur ce thème...
                </div>

                <?php
            } else {

                while ($row = mysqli_fetch_array($result)) {
                    ?>

                    <div class="post">
                        <div class="card mb-3">
                            <h3 class="card-header">Post de <?php echo($row["nomProfil"]); ?>
                                dans <?php echo($row["libelleTheme"]); ?> - <a
                                        href="./post.php?idPost=<?php echo($row["idMessage"]); ?>">Voir le post seul</a>
                            </h3>

                            <div class="card-body">

                                <p class="card-text"><?php echo($row["contenuMessage"]); ?></p>

                                <small><span
                                            id="like-<?php echo($row["idMessage"]); ?>"><?php echo(getNbLikeOnMessageById($row["idMessage"])); ?></span>
                                    j'aime
                                </small>
                            </div>

                            <div class="card-body">
                                <?php if (doILikeThis($row["idMessage"])) { ?>
                                    <a class="btn btn-outline-success" id="<?php echo($row["idMessage"]); ?>"
                                       onclick="likeMessage(<?php echo($row["idMessage"]); ?>)">Je n'aime plus</a>
                                <?php } else { ?>
                                    <a class="btn btn-outline-primary" id="<?php echo($row["idMessage"]); ?>"
                                       onclick="likeMessage(<?php echo($row["idMessage"]); ?>)">J'aime </a>
                                <?php } ?>


                            </div>

                            <div class="card-body">

                                <h4>Réponses du post</h4>


                                <?php

                                $reponses = mysqli_query($bdd, "SELECT * FROM message JOIN profil p on message.idProfil = p.idProfil WHERE idMessage_1 IS NOT NULL AND idMessage_1 = " . $row["idMessage"] . " ORDER BY idMessage DESC;");

                                if ($reponses->num_rows == 0) {

                                    ?>

                                    <div class="alert alert-warning"><strong>Oops... </strong> Aucune réponse...
                                    </div>

                                    <?php

                                } else {

                                    while ($rowRep = mysqli_fetch_array($reponses)) {
                                        ?>

                                        <div class="list-group-item list-group-item-action flex-column align-items-start">
                                            <div class="d-flex w-100 justify-content-between">
                                                <h5 class="mb-1"><?php echo($rowRep["nomProfil"]); ?>
                                                </h5>
                                                <small class="text-muted"><?php echo(explode(" ", $rowRep["timestampMessage"])[0]); ?>
                                                    à <?php echo(explode(" ", $rowRep["timestampMessage"])[1]); ?></small>
                                            </div>
                                            <p class="mb-1"><?php echo($rowRep["contenuMessage"]); ?></p>
                                            <small><span
                                                        id="like-<?php echo($rowRep["idMessage"]); ?>"><?php echo(getNbLikeOnMessageById($rowRep["idMessage"])); ?></span>
                                                j'aime
                                            </small>
                                            <div class="card-body">
                                                <?php if (doILikeThis($rowRep["idMessage"])) { ?>
                                                    <a class="btn btn-outline-success btn-sm"
                                                       id="<?php echo($rowRep["idMessage"]); ?>"
                                                       onclick="likeMessage(<?php echo($rowRep["idMessage"]); ?>)">Je
                                                        n'aime
                                                        plus
                                                    </a>

                                                <?php } else { ?>
                                                    <a class="btn btn-outline-primary btn-sm"
                                                       id="<?php echo($rowRep["idMessage"]); ?>"
                                                       onclick="likeMessage(<?php echo($rowRep["idMessage"]); ?>)">J'aime
                                                    </a>
                                                <?php } ?>

                                            </div>

                                            <div class="card-body">

                                                <h4>En réponse à ce commentaire</h4>


                                                <?php

                                                $reponsesRep = mysqli_query($bdd, "SELECT * FROM message JOIN profil p on message.idProfil = p.idProfil WHERE idMessage_1 IS NOT NULL AND idMessage_1 = " . $rowRep["idMessage"] . " ORDER BY idMessage DESC;");

                                                if ($reponsesRep->num_rows == 0) {

                                                    ?>

                                                    <div class="alert alert-warning"><strong>Oops... </strong> Aucune
                                                        réponse...
                                                    </div>

                                                    <?php

                                                } else {

                                                    while ($rowRepRep = mysqli_fetch_array($reponsesRep)) {
                                                        ?>

                                                        <div class="list-group-item list-group-item-action flex-column align-items-start">
                                                            <div class="d-flex w-100 justify-content-between">
                                                                <h5 class="mb-1"><?php echo($rowRepRep["nomProfil"]); ?>
                                                                </h5>
                                                                <small class="text-muted"><?php echo(explode(" ", $rowRepRep["timestampMessage"])[0]); ?>
                                                                    à <?php echo(explode(" ", $rowRepRep["timestampMessage"])[1]); ?></small>
                                                            </div>
                                                            <p class="mb-1"><?php echo($rowRepRep["contenuMessage"]); ?></p>
                                                            <small><span
                                                                        id="like-<?php echo($rowRepRep["idMessage"]); ?>"><?php echo(getNbLikeOnMessageById($rowRepRep["idMessage"])); ?></span>
                                                                j'aime
                                                            </small>
                                                            <div class="card-body">
                                                                <?php if (doILikeThis($rowRepRep["idMessage"])) { ?>
                                                                    <a id="<?php echo($rowRepRep["idMessage"]); ?>"
                                                                       class="btn btn-outline-success btn-sm"
                                                                       onclick="likeMessage(<?php echo($rowRepRep["idMessage"]); ?>)">Je
                                                                        n'aime plus</a>
                                                                <?php } else { ?>
                                                                    <a id="<?php echo($rowRepRep["idMessage"]); ?>"
                                                                       class="btn btn-outline-primary btn-sm"
                                                                       onclick="likeMessage(<?php echo($rowRepRep["idMessage"]); ?>)">J'aime</a>
                                                                <?php } ?>

                                                            </div>


                                                        </div>

                                                    <?php }
                                                } ?>


                                            </div>


                                        </div>

                                    <?php }
                                } ?>


                            </div>
                            <div class="card-footer text-muted">
                                Posté le <?php echo(explode(" ", $row["timestampMessage"])[0]); ?>
                                à <?php echo(explode(" ", $row["timestampMessage"])[1]); ?>
                            </div>
                        </div>
                    </div>

                <?php }
            } ?>


        </article>

    </section>


<?php } ?>

<script>

    var likeMsg = new Audio('/assets/like.ogg');
    var dislikeMsg = new Audio('/assets/dislike.ogg');

    function likeMessage(idMessage) {
        $.post("/social/ajax/like.php",
            {
                idMessage: idMessage,
            },

            function (data, status) {

                data = JSON.parse(data);

                if (data["success"] === true) {

                    document.getElementById(idMessage).textContent = data["newText"];
                    document.getElementById("like-" + idMessage).textContent = data["nbLike"];

                    if(data["newText"] === "Je n'aime plus"){

                        likeMsg.play();
                    }else{

                        dislikeMsg.play();
                    }

                }


            }
        );
    }
</script>


<?php include(__DIR__ . "/../inc/footer.php"); ?>

</body>
</html>
