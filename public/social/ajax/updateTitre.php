<?php

@session_start();

require_once(__DIR__."/../../php/functions/user.php");
require_once(__DIR__."/../../php/functions/themes.php");
require_once(__DIR__."/../../php/database/connect.php");

if(!isLoggedIn()){
    die("Not logged in...");
}

if(!isset($_POST["idTitre"])){
    $_SESSION["updateTitre"] = "<div class='alert alert-danger'><strong>Opps !</strong> Requête incorrecte... </div>";
    header("Location: /settings.php");
    die();
}
if(empty($_POST["idTitre"]) || $_POST["idTitre"] == 0){
    $_SESSION["updateTitre"] = "<div class='alert alert-danger'><strong>Opps !</strong> Titre invalide ... </div>";
    header("Location: /settings.php");
    die();
}

$idTitre = htmlspecialchars(addslashes($_POST["idTitre"]));
$idProfil = $_SESSION["idProfil"];


$result = mysqli_query($bdd, "SELECT * FROM remporter WHERE idProfil = ".$idProfil." AND idTitre = ".$idTitre.";");


if($result->num_rows == 0){


    $_SESSION["updateTitre"] = "<div class='alert alert-danger'><strong>Opps !</strong> Vous n'avez pas gagné ce titre ... </div>";
    header("Location: /settings.php");
    die();

}else{


    $update = mysqli_query($bdd, "UPDATE profil SET idTitre = ".$idTitre." WHERE idProfil = ".$idProfil.";");
    if(!mysqli_affected_rows($bdd)){


        $_SESSION["updateTitre"] = "<div class='alert alert-danger'><strong>Opps !</strong> Une erreur s'est produite... </div>";
        header("Location: /settings.php");
        die();

    }else{


        $_SESSION["updateTitre"] = "<div class='alert alert-success'><strong>Cool !</strong> Votre titre à été mis à jour ! </div>";
        header("Location: /settings.php");
        die();

    }

}