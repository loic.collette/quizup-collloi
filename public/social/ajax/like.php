<?php
/**
 * Created by PhpStorm.
 * User: collloi
 * Date: 19/03/19
 * Time: 15:13
 */

require_once(__DIR__."/../../php/database/connect.php");
require_once(__DIR__."/../../php/functions/json.php");
require_once(__DIR__."/../../php/functions/user.php");
require_once(__DIR__."/../../php/functions/social.php");

@session_start();

$answer = array();

if(!isLoggedIn()){

    $answer["success"] = false;
    $answer["newText"] = "Non connecté";

    die(encodeAndSendJson($answer));
}

if(!isset($_POST["idMessage"]) || empty($_POST["idMessage"])){

    $answer["success"] = false;
    $answer["newText"] = "Erreur";

    die(encodeAndSendJson($answer));


}

$idMessage = addslashes(htmlspecialchars($_POST["idMessage"]));


$result = mysqli_query($bdd, "SELECT * FROM message WHERE idMessage = ".$idMessage.";");

if($result->num_rows == 0){

    $answer["success"] = true;
    $answer["newText"] = "Message inexistant";
    die(encodeAndSendJson($answer));

}


$nbLike = getNbLikeOnMessageById($idMessage);

$result = mysqli_query($bdd, "SELECT * FROM liker WHERE idProfil = ".$_SESSION["idProfil"]." AND idMessage = ".$idMessage.";");

if($result->num_rows == 0){


    $like = mysqli_query($bdd, "INSERT INTO liker VALUES (".$_SESSION["idProfil"].", ".$idMessage.");");
    if(mysqli_affected_rows($bdd)){


        $answer["success"] = true;
        $answer["newText"] = "Je n'aime plus";
        $answer["nbLike"] = $nbLike + 1;


    }else{


        $answer["success"] = true;
        $answer["newText"] = "Erreur";
        $answer["nbLike"] = "?";


    }

}else{

    $like = mysqli_query($bdd, "DELETE FROM liker WHERE idProfil = ".$_SESSION["idProfil"]." AND idMessage = ".$idMessage.";");
    if(mysqli_affected_rows($bdd)){


        $answer["success"] = true;
        $answer["newText"] = "J'aime";
        $answer["nbLike"] = $nbLike - 1;


    }else{


        $answer["success"] = true;
        $answer["newText"] = "Erreur";
        $answer["nbLike"] = "?";


    }

}

encodeAndSendJson($answer);