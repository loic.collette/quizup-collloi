<?php


require_once(__DIR__ . "/../../php/database/connect.php");
require_once(__DIR__ . "/../../php/functions/user.php");
require_once(__DIR__ . "/../../php/functions/json.php");




@session_start();

$status = array();


if(!isLoggedIn()){

    $status["success"] = false;
    $status["message"] = '<div class="alert alert-primary"><strong>Quoi ?</strong>Tu n\'est pas connecté...</div>';
    die(encodeAndSendJson($status));

}

$idProfil = $_SESSION["idProfil"];
$messageTo = addslashes(htmlspecialchars($_POST["idUser"]));

if(!doesThisPlayerExist($messageTo)){

    $status["success"] = false;
    $status["message"] = '<div class="alert alert-primary"><strong>Hum...</strong>Ce joueur n\'existe pas...</div>';
    die(encodeAndSendJson($status));

}


if(isHeBlockedByUser($idProfil, $messageTo)){

    $status["success"] = false;
    $status["message"] = '<div class="alert alert-primary"><strong>Oh... </strong>Ce joueur t\'a bloqué !</div>';
    die(encodeAndSendJson($status));

}

$message = addslashes(htmlspecialchars($_POST["messageUser"]));

if(empty($message)){

    $status["success"] = true;
    $status["message"] = '<div class="alert alert-primary"><strong>Oh... </strong>Ton message est vide.</div>';
    die(encodeAndSendJson($status));

}

//$messageOk = trim(preg_replace('/[\t\n\r\s]+/', ' ', $message));

$time = date("Y/m/d H:i:s", time());

$result = mysqli_query($bdd, "INSERT INTO `chat_msg` VALUES (NULL, '".$time."', '".$message."', 0, ".$messageTo.", ".$idProfil.");");



if($result){

    $status["success"] = true;
    $status["message"] = '<div class="alert alert-success"><strong>Yep !</strong>Message envoyé !</div>';

}else{


    $status["success"] = false;
    $status["message"] = '<div class="alert alert-primary"><strong>Ouch !</strong>Un problème s\'est produit lors de l\'envoi du message !</div>';

}

die(encodeAndSendJson($status));