<?php

require_once(__DIR__ . "/../../php/database/connect.php");
require_once(__DIR__ . "/../../php/functions/user.php");
require_once(__DIR__ . "/../../php/functions/json.php");


@session_start();

$status = array();


if (!isLoggedIn()) {

    $status["success"] = false;
    $status["listMsg"][0] = '<div class="alert alert-primary"><strong>Quoi ? </strong>Tu n\'est pas connecté...</div>';
    die(encodeAndSendJson($status));

}

$userId = addslashes(htmlspecialchars($_POST["idUser"]));
$idProfil = $_SESSION["idProfil"];

if (!doesThisPlayerExist($userId)) {

    $status["success"] = true;
    $status["listMsg"][0] = '<div class="alert alert-primary"><strong>Hum... </strong>Ce joueur n\'existe pas...</div>';
    die(encodeAndSendJson($status));

}

if ($userId == "0") {

    $status["success"] = true;
    $status["listMsg"][0] = '<div class="alert alert-secondary"><strong>Hum... </strong> Choisis une conversation.</div>';
    die(encodeAndSendJson($status));

}
if ($userId == $idProfil) {

    $status["success"] = true;
    $status["listMsg"][0] = '<div class="alert alert-danger"><strong>Hum... </strong> Je sais pas comment tu as fais, mais tu peux pas te choisir.</div>';
    die(encodeAndSendJson($status));

}


$result = mysqli_query($bdd, "SELECT * FROM `chat_msg` WHERE `idProfil_emetteur` = " . $idProfil . " AND `idProfil_recepteur` = " . $userId . " OR `idProfil_emetteur` = " . $userId . " AND `idProfil_recepteur` = " . $idProfil . ";");

$status["listMsg"] = array();
$status["username"] = getProfileDetails($userId)["details"]["username"];

$i = 0;
if ($result->num_rows <= 0) {

    $status["success"] = true;
    $status["listMsg"][$i] = '<div class="alert alert-dismissible alert-secondary"><strong>Hum...</strong> Aucun message dans cette conversation.</div>';



} else {

    while ($row = mysqli_fetch_array($result)) {

        $username = getProfileDetails($row["idProfil_emetteur"])["details"]["username"];

        if ($idProfil != $row["idProfil_emetteur"]) {
            $class = "active";
            if ($row["lu"] == '0') {
                mysqli_query($bdd, "UPDATE chat_msg SET lu = '1' WHERE idChatMsg = " . $row["idChatMsg"] . " AND idProfil_recepteur = ".$_SESSION["idProfil"].";");
                echo(mysqli_error($bdd));
                die();
            }
        } else {
            $class = "";
        }

        $status["listMsg"][$i] = '<a href="#" class="list-group-item list-group-item-action flex-column align-items-start ' . $class . '"><div class="d-flex w-100 justify-content-between"><h5 class="mb-1">' . $username . '</h5><small class="text-muted">' . $row["timestampMsg"] . '</small></div><p class="mb-1">' . $row["contenu"] . '</p></a>';

        $i++;

    }


    $status["message"] = '<div class="alert alert-success"><strong>Yep ! </strong>Conversation chargée...</div>';
    $status["success"] = true;
}

encodeAndSendJson($status);