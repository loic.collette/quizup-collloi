<?php
/**
 * Created by PhpStorm.
 * User: collloi
 * Date: 11/03/19
 * Time: 12:28
 *
 *
 */

require_once(__DIR__."/php/functions/user.php");

?>

<html>
<head>
    <title>Accueil</title>
    <?php require(__DIR__."/inc/head.php"); ?>
</head>
<body>

<?php require(__DIR__."/inc/nav.php"); ?>

<section>
    

    <div class="jumbotron">
        <h1 class="display-3">QuizUp</h1>
        <p class="lead">Le plus grand jeu de questions-réponses au monde</p>
        <hr class="my-4">
        <p>Un jeu de culture générale en ligne amusant et stimulant qui tire le meilleur parti de deux univers.</p>
        <p class="lead">
            <?php if(!isLoggedIn()){ ?>
                <a class="btn btn-primary btn-lg" href="/login.php" role="button">Se connecter</a>
                <a class="btn btn-primary btn-lg" href="/register.php" role="button">S'inscrire</a>
            <?php } else {?>
                <a class="btn btn-primary btn-lg" href="/game/index.php" role="button">Jouer !</a>
            <?php } ?>
        </p>
    </div>

</section>

<section>
    <header>
        <h2>Les topics les plus joués</h2>
    </header>

    <div class="row">

        <?php


        $topPartie = mysqli_query($bdd, "SELECT count(*)/7 as nbParties , t.idTheme, t.libelleTheme, t.description FROM participer LEFT JOIN partie p on participer.idPartie = p.idPartie LEFT JOIN integrer i on p.idPartie = i.idPartie LEFT JOIN question q on i.idQuestion = q.idQuestion LEFT JOIN theme t on q.idTheme = t.idTheme GROUP BY t.idTheme ORDER BY nbParties DESC LIMIT 6");

        while ($row = mysqli_fetch_array($topPartie)) {
            ?>

            <div class="col-sm-6">
                <div class="card">
                    <div class="card-body">
                        <h5 class="card-title"><?php echo($row["libelleTheme"]); ?> - <?php echo((int) $row["nbParties"]); ?> partie(s)</h5>
                      <!-- je caste en int car sinon mysql me renvoie un nombre à virgule -->
                        <p class="card-text"><?php echo($row["description"]); ?></p>
                        <a href="/game/voir.php?idTheme=<?php echo($row["idTheme"]); ?>" class="btn btn-primary">Jouer</a>
                    </div>
                </div>
            </div>
        <?php } ?>


    </div>

</section>





<?php include(__DIR__."/inc/footer.php"); ?>

</body>
</html>
