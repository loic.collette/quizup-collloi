<?php


require_once(__DIR__ . "/functions/user.php");
require_once(__DIR__ . "/database/connect.php");

@session_start();

if (!isLoggedIn() || !isset($_FILES["backgroundPic"])) {
    die();
}

$target_dir = __DIR__ . "/../img/users/facade/"; // dossier de l'upload
$target_file = $target_dir . basename($_FILES["backgroundPic"]["name"]); // dossier upload + filename
$target_file_to_upload = $target_dir . $_SESSION["idProfil"]; // dossier upload + filename



$uploadOk = 0; // on assume que l'image est pas correcte
$imageFileType = strtolower(pathinfo($target_file, PATHINFO_EXTENSION));

// Check if image file is a actual image or fake image


$check = getimagesize($_FILES["backgroundPic"]["tmp_name"]);


if ($check !== false) {

    $uploadOk = 1;

} else {

    $_SESSION["imgStatus"] = "<div class='alert alert-primary'><strong>Oops !</strong> L'image envoyée en n'est pas une... sois sympa...</div>";
    header("Location: ../settings.php");
    die();


}

/*if($check[0] != 128 || $check[1] != 128){

    $_SESSION["imgStatus"] = "<div class='alert alert-primary'><strong>Oops !</strong> L'image du profil doit faire 128 pixel sur 128 pixel.</div>";
    header("Location: ../settings.php");
    die();

}*/

// Check if file already exists
if (file_exists($target_file)) {
    unlink($target_file);
}
// Check file size
if ($_FILES["backgroundPic"]["size"] > 500000) { // pas plus de 500ko...
    $_SESSION["imgStatus"] = "<div class='alert alert-primary'><strong>Oops !</strong> L'image envoyée est trop grosse... (max. 500 Ko)</div>";
    header("Location: ../settings.php");
    die();
}
// Allow certain file formats
if ($imageFileType != "jpg" && $imageFileType != "png" && $imageFileType != "jpeg"
    && $imageFileType != "gif") {



    $_SESSION["imgStatus"] = "<div class='alert alert-primary'><strong>Oops !</strong> Les extensions de fichier autorisée sont PNG, JP(E)G, GIF.</div>";
    header("Location: ../settings.php");
    die();

}
// Check if $uploadOk is set to 0 by an error
if ($uploadOk == 1) {
    if (move_uploaded_file($_FILES["backgroundPic"]["tmp_name"], $target_file_to_upload.".".$imageFileType)) {


        $url = "/img/users/facade/" . $_SESSION["idProfil"] . "." . $imageFileType;

        mysqli_query($bdd, "UPDATE profil SET photoFacade = '".$url."' WHERE idProfil = ".$_SESSION["idProfil"].";");

        $_SESSION["imgStatus"] = "<div class='alert alert-success'><strong>Yaas !</strong> Image mise à jour !</div>";
    } else {
        $_SESSION["imgStatus"] = "<div class='alert alert-primary'><strong>Aïe !</strong> Une erreur interne s'est produite...</div>";
    }


    header("Location: ../settings.php");

}
?>