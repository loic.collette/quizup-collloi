<?php
/**
 * Created by PhpStorm.
 * User: collloi
 * Date: 12/03/19
 * Time: 14:22
 */


$regex = array(
    "email" => "#^[0-9a-zA-Z._-]{2,}@[a-zA-Z.-]{2,}\.[a-zA-Z]{2,3}$#",
    "password" => "#^(?=.*[0-9])(?=.*[A-Z]).{8,999}$#",
    "passwordc" => "#^(?=.*[0-9])(?=.*[A-Z]).{8,999}$#",
    "username" => "#.{3,}#", // au moins 3 caractère mais n'importe
    "any" => "#.*#",
    "lang" => "#.*#",
    "ville" => "#.*#",
    "bio" => "#.*#",
    "prive" => "#[true|false]#",
);


function checkInput($input, $key){
    global $regex;

    if(preg_match($regex[$key], $input)){
        return true;
    }
    return false;
}