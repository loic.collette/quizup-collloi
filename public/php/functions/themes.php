<?php
/**
 * Created by PhpStorm.
 * User: collloi
 * Date: 19/03/19
 * Time: 19:36
 */

require_once(__DIR__ . "/../../php/database/connect.php");
require_once(__DIR__ . "/../../php/functions/user.php");


function getAllCategories()
{

    global $bdd;
    $result = mysqli_query($bdd, "SELECT * FROM `categorie`");
    $answer = array();

    $i = 0;
    while ($row = mysqli_fetch_array($result)) {
        $answer[$i++] = array(
            "id" => $row["idCategorie"],
            "nom" => $row["libelleCategorie"],
        );
    }

    return $answer;

}

function countThemeByCategorieId($id)
{

    global $bdd;
    $result = mysqli_query($bdd, "SELECT count(*) as nbTheme FROM `theme` WHERE `idcategorie` = " . $id . ";");
    $nbTheme = mysqli_fetch_array($result);
    return $nbTheme["nbTheme"];

}

function countQuestionByThemeId($id)
{


    global $bdd;
    $result = mysqli_query($bdd, "SELECT count(*) as nbQuest FROM `question` WHERE `idtheme` = " . $id . ";");
    $nbQuest = mysqli_fetch_array($result);
    return $nbQuest["nbQuest"];

}

function getAllThemeByCategorieId($id)
{

    global $bdd;
    $result = mysqli_query($bdd, "SELECT * FROM `theme` WHERE `idcategorie` = " . $id . ";");

    $answer = array();

    if ($result->num_rows) {
        $i = 0;
        while ($row = mysqli_fetch_array($result)) {
            $answer[$i++] = array(
                "id" => $row["idTheme"],
                "nom" => $row["libelleTheme"],
                "description" => $row["description"],
                "logo" => $row["logo"],
                "lastUpdate" => $row["dateUpdated"],
                "idProfil" => $row["idProfil"],
            );
        }
    }

    return $answer;

}

function getThemeDetailsById($id)
{


    global $bdd;
    $result = mysqli_query($bdd, "SELECT * FROM `theme` WHERE `idtheme` = " . $id . ";");


    if ($result->num_rows) {
        $row = mysqli_fetch_array($result);
        $themeDetails = array(
            "success" => true,
            "details" => array(
                "id" => $row["idTheme"],
                "nom" => $row["libelleTheme"],
                "description" => $row["description"],
                "logo" => $row["logo"],
                "lastUpdate" => $row["dateUpdated"],
                "idProfil" => getProfileDetails($row["idProfil"]),
            ),
        );
    }else{
        return array(
            "success" => false,
            "message" => "Le thème demandé n'a pas été trouvé.",
        );
    }

    return $themeDetails;

}

function getCategorieByThemeId($id){

    global $bdd;
    $result = mysqli_query($bdd, "SELECT * FROM `theme` JOIN `categorie` ON (`theme`.`idcategorie` = `categorie`.`idcategorie`) WHERE `idtheme` = " . $id . ";");


    if ($result->num_rows) {
        $row = mysqli_fetch_array($result);
        $cateDetails = array(
            "success" => true,
            "details" => array(
                "id" => $row["idCategorie"],
                "nom" => $row["libelleCategorie"],
            ),
        );
    }else{
        return array(
            "success" => false,
            "message" => "Le thème demandé n'a pas été trouvé.",
        );
    }

    return $cateDetails;

}

function doesThisCategorieExist($id){

    global $bdd;
    $result = mysqli_query($bdd, "SELECT * FROM categorie WHERE idCategorie = ".$id.";");


    if ($result->num_rows) {
        return true;
    }
    return false;

}


function doIfollowThisTheme($id){

    global $bdd;
    @session_start();

    $result = mysqli_query($bdd, "SELECT * FROM suivre WHERE idProfil = ".$_SESSION["idProfil"]." AND idTheme = ".$id.";");
    if($result->num_rows){
        return true;
    }
    return false;


}


function doesThisThemeExist($id){

    global $bdd;

    $result = mysqli_query($bdd, "SELECT * FROM theme WHERE idTheme = ".$id.";");
    if($result->num_rows){
        return true;
    }
    return false;

}


function getTop5PlaysFromThemeId($id){

    global $bdd;

    $result = mysqli_query($bdd, "SELECT profil.nomProfil, r.idProfil, sum(r.points) 
FROM profil
JOIN repondre r 
    on profil.idProfil = r.idProfil
JOIN question q 
    on r.idQuestion = q.idQuestion
WHERE idTheme = ".$id."
GROUP BY r.idProfil, profil.nomProfil
ORDER BY sum(r.points) DESC
LIMIT 5");

    $i = 0;

    $classement = array();

    if($result->num_rows){

        while($row = mysqli_fetch_array($result)){

            $classement[$i]["id"] = $row["idProfil"];
            $classement[$i]["username"] = $row["nomProfil"];
            $classement[$i]["points"] = number_format(calculThemeLevel($row["idProfil"], $id)["total"], 2);

            $i++;

        }

    }else{

        $classement[$i]["id"] = "??";
        $classement[$i]["username"] = "??";
        $classement[$i]["points"] = "??";


    }

    return $classement;

}

function doesThisThemeBelongToThisUser($id){

    @session_start();
    global $bdd;

    if(!isLoggedIn()){
      return false;
    }
  
  // oublié, provoque une erreur lors de l'affichage des thèmes.
  
    $result = mysqli_query($bdd, "SELECT * FROM theme WHERE idTheme = ".$id.";");
    if($result->num_rows > 0){

        $idUser = $result->fetch_array();

        if($idUser["idProfil"] == $_SESSION["idProfil"]){
            return true;
        }else{
            return false;
        }


    }else{
        return false;
    }


}