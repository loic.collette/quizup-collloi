<?php
/**
 * Created by PhpStorm.
 * User: collloi
 * Date: 11/03/19
 * Time: 12:28
 *
 *
 */

require_once(__DIR__ . "/php/functions/user.php");

redirectIfnotLoggedIn()
?>

<html>
<head>
    <title>Accueil</title>
    <?php require(__DIR__ . "/inc/head.php"); ?>
</head>
<body>

<?php require(__DIR__ . "/inc/nav.php"); ?>

<section>

    <div id="serverAnswer">

    </div>

    <?php

    // todo : permettre de modifier le nom d'utilisateur
    $fields = array(
        array(
            "id" => "username",

        ),
        array(
            "id" => "ville",

        ),
        array(
            "id" => "bio",

        ),
        array(
            "id" => "lang",

        ),
        array(
            "id" => "prive",

        ),
    );


    $langue = array(
        "Français",
        "Anglais",
        "Neerlandais",
        "Almemand",
        "Espagnol",
    );


    $details = getProfileDetails($_SESSION["idProfil"]);

    ?>

    <div id="serverAnswer">

    </div>

    <fieldset>
        <legend>Votre profil</legend>

        <!--

        Impossible de faire un formulaire personnel, car utilisation trop faible dans le site

        -->
        <div class="form-group">
            <label for="username">Nom d'utilisateur</label>
            <input type="text" class="form-control" id="username" placeholder="Nom d'utilisateur"
                   value="<?php echo($details["details"]["username"]); ?>">
        </div>

        <div class="form-group">
            <label for="ville">Ville</label>
            <input type="text" class="form-control" id="ville" placeholder="Votre ville"
                   value="<?php echo($details["details"]["ville"]); ?>">
        </div>

        <div class="form-group">
            <label for="bio">Biographie</label>
            <textarea class="form-control" id="bio" rows="3"><?php echo($details["details"]["bio"]); ?></textarea>
        </div>

        <div class="form-group">
            <div class="custom-control custom-switch">
                <input type="checkbox" class="custom-control-input"
                       id="prive" <?php if ($details["details"]["private"]) {
                    echo("checked=''");
                } ?>>
                <label class="custom-control-label" for="prive">Profil privé</label>
            </div>
        </div>

        <div class="form-group">
            <label for="lang">Langue (ceci ne changera rien...)</label>
            <select class="custom-select" id="lang">
                <option disabled selected="" value="none">Chiosissez une langue</option>
                <?php foreach ($langue as $option) { ?>
                    <option value="<?php echo($option); ?>" <?php if ($details["details"]["langue"] == $option) {
                        echo("selected=''");
                    } ?>>
                        <?php echo($option); ?>
                    </option>
                <?php } ?>
            </select>
        </div>

        <button type="submit" class="updateProfile btn btn-primary">Sauvegarder</button>

    </fieldset>


</section>


<?php generateAjax("updateProfile", $fields, "/php/updateProfile.php", 2000, "/social/profile.php"); ?>


<section>

    <h2>Mettre à jour mes photos</h2>

    <div>
        <?php echo(@$_SESSION["imgStatus"]);
        $_SESSION["imgStatus"] = ""; ?>
    </div>

    <h4>Mettre à jour la photo de profil</h4>

    <form action="./php/updateProfilePic.php" method="post" enctype="multipart/form-data">
        <div class="form-group">
            <div class="input-group mb-3">
                <div class="custom-file">
                    <input type="file" class="custom-file-input" name="profilePic" id="inputGroupFile01">
                    <label class="custom-file-label" for="inputGroupFile01">Choisir une nouvelle photo de profil
                        (128x128, 500ko max.)</label>
                </div>
                <div class="input-group-append">
                    <input type="submit" class="btn btn-sm btn-primary" value="Envoyer !">
                </div>
            </div>
        </div>
    </form>


    <h4>Mettre à jour la photo de couverture</h4>

    <form action="./php/updateBackgroundPic.php" method="post" enctype="multipart/form-data">
        <div class="form-group">
            <div class="input-group mb-3">
                <div class="custom-file">
                    <input type="file" class="custom-file-input" name="backgroundPic" id="inputGroupFile02">
                    <label class="custom-file-label" for="inputGroupFile02">Choisir une nouvelle photo de
                        couverture</label>
                </div>
                <div class="input-group-append">
                    <input type="submit" class="btn btn-sm btn-primary" value="Envoyer !">
                </div>
            </div>
        </div>
    </form>


</section>

<section>

    <h2>
        Titre du profil
    </h2>

    <div>
        <?php echo(@$_SESSION["updateTitre"]); $_SESSION["updateTitre"] = ""; ?>
    </div>

    <?php

    $titres = getAllWonTitlesByUserId($_SESSION["idProfil"]);
    //die(var_dump($titres));

    ?>

    <form method="post" action="/social/ajax/updateTitre.php">

    <div class="form-group">
        <label for="titre">Titre</label>
        <select class="custom-select" id="titre" name="idTitre">
            <option disabled selected="" value="0">Chiosissez un titre</option>
            <?php foreach ($titres as $titre) { ?>
                <option value="<?php echo($titre["id"]); ?>" <?php if ($titre["selected"]) { echo("selected=''"); } ?>>
                    <?php echo($titre["categorie"]." - ".$titre["theme"]." - ".$titre["titre"]." - Niv. ".$titre["level"]); ?>
                </option>
            <?php } ?>
        </select>
    </div>

    <input type="submit" class="btn btn-primary updateTitre" value="Mettre à jour !">

    </form>

</section>

<section>

    <h2>Profils bloqués</h2>
    <table class="table table-hover">
        <thead>
        <tr>
            <th scope="col">Avatar</th>
            <th scope="col">Nom d'utilisateur</th>
            <th scope="col">Débloquer</th>
        </tr>
        </thead>
        <tbody>
        <?php
        $listBlocked = getAllBlockedProfileByUser();
        foreach ($listBlocked as $user) {
            ?>

            <tr>

                <td><img alt="<?php echo($user["username"]); ?>" src="<?php echo($user["image"]); ?>"/></td>
                <td><?php echo($user["username"]); ?></td>
                <td>
                    <button type="button" class="btn btn-success" id="unblock-<?php echo($user["id"]); ?>"
                            value="<?php echo($user["id"]); ?>">Débloquer
                    </button>
                </td>
                <script>

                    $("#unblock-<?php echo($user["id"]); ?>").click(function () {
                        $.post("/social/ajax/unblock.php",
                            {
                                idUser: $('#unblock-<?php echo($user["id"]); ?>').val(),
                            },
                            function (data, status) {

                                data = JSON.parse(data);
                                document.getElementById("serverAnswer").innerHTML = data["message"];

                                var element = document.getElementById("serverAnswer");
                                if (data["success"]) {
                                    if (document.getElementById("serverAnswer").classList.contains('alert-danger')) {
                                        document.getElementById("serverAnswer").classList.remove('alert-danger');
                                    }
                                    element.classList.add("alert-success");
                                    document.getElementById("unblock-<?php echo($user["id"]); ?>").style.visibility = 0;
                                } else {
                                    element.classList.add("alert-danger");
                                }

                            });
                    });

                </script>
            </tr>


        <?php } ?>
        </tbody>
    </table>

</section>



<?php include(__DIR__ . "/inc/footer.php"); ?>

</body>
</html>
