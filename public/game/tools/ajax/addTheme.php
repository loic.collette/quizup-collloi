<?php


require_once(__DIR__ . "/../../../php/database/connect.php");
require_once(__DIR__ . "/../../../php/functions/user.php");
require_once(__DIR__ . "/../../../php/functions/json.php");
require_once(__DIR__ . "/../../../php/functions/themes.php");

if (!isLoggedIn()) {
    die();
}

$idCate = htmlspecialchars(addslashes($_POST["idCate"]));

if(!doesThisCategorieExist($idCate)){

    die();

}

@session_start();

$_SESSION["addTheme"] = array();

if (!isset($_POST["nom"]) || !isset($_POST["description"]) || !isset($_FILES["logoTheme"])) {

    $_SESSION["addTheme"] = "<div class='alert alert-primary'><strong>Oops !</strong> Requête incorecte !</div>";
    header("Location: ../addTheme.php?idCate=" . $idCate . "");
    die();
}

if (empty($_POST["nom"])) {

    $_SESSION["addTheme"] = "<div class='alert alert-primary'><strong>Oops !</strong> Nom vide !</div>";
    header("Location: ../addTheme.php?idCate=" . $idCate . "");
    die();
}

if (empty($_POST["description"])) {

    $_SESSION["addTheme"] = "<div class='alert alert-primary'><strong>Oops !</strong> Description vide !</div>";
    header("Location: ../addTheme.php?idCate=" . $idCate . "");
    die();
}

$nom = htmlspecialchars(addslashes($_POST["nom"]));
$description = htmlspecialchars(addslashes($_POST["description"]));
$now = date("Y-m-d H:i:s", time());

$idCate = htmlspecialchars(addslashes($_POST["idCate"]));

if (!doesThisCategorieExist($idCate)) {
    header("Location: /game/");
    die();
}

// IMAGE IMAGE IMAGE


$target_dir = __DIR__ . "/../../../img/themes/"; // dossier de l'upload

$target_file = $target_dir . basename($_FILES["logoTheme"]["name"]); // dossier upload + filename
$idunique = uniqid();
$target_file_to_upload = $target_dir . $idunique; // dossier upload + timestamp unique


$uploadOk = 0; // on assume que l'image est pas correcte
$imageFileType = strtolower(pathinfo($target_file, PATHINFO_EXTENSION));

// Check if image file is a actual image or fake image


$check = getimagesize($_FILES["logoTheme"]["tmp_name"]);


if ($check !== false) {

    $uploadOk = 1;

} else {

    $_SESSION["addTheme"] = "<div class='alert alert-primary'><strong>Oops !</strong> L'image envoyée en n'est pas une... sois sympa...</div>";
    header("Location: ../addTheme.php?idCate=" . $idCate . "");
    die();


}

/*if($check[0] != 128 || $check[1] != 128){

    $_SESSION["addTheme"] = "<div class='alert alert-primary'><strong>Oops !</strong> L'image du profil doit faire 128 pixel sur 128 pixel.</div>";
    header("Location: ../addTheme.php");
    die();

}
*/
// Check if file already exists
if (file_exists($target_file)) {
    unlink($target_file);
}
// Check file size
if ($_FILES["logoTheme"]["size"] > 500000) { // pas plus de 500ko...
    $_SESSION["addTheme"] = "<div class='alert alert-primary'><strong>Oops !</strong> L'image envoyée est trop grosse... (max. 500 Ko)</div>";
    header("Location: ../addTheme.php?idCate=" . $idCate . "");
    die();
}
// Allow certain file formats
if ($imageFileType != "jpg" && $imageFileType != "png" && $imageFileType != "jpeg"
    && $imageFileType != "gif") {


    $_SESSION["addTheme"] = "<div class='alert alert-primary'><strong>Oops !</strong> Les extensions de fichier autorisée sont PNG, JP(E)G, GIF.</div>";
    header("Location: ../addTheme.php?idCate=" . $idCate . "");
    die();

}
// Check if $uploadOk is set to 0 by an error
if ($uploadOk == 1) {
    if (move_uploaded_file($_FILES["logoTheme"]["tmp_name"], $target_file_to_upload . "." . $imageFileType)) {


        $url = $target_file_to_upload . "." . $imageFileType;
        $url = explode("game/tools/ajax/../../..", $url)[1];


        // todo : a ameliorer !!

        //die($url);

        $result = mysqli_query($bdd, "SELECT * FROM theme WHERE libelleTheme = '" . $nom . "' WHERE idCategorie = " . $idCate . ";");

        if ($result->num_rows >= 1) {

            $_SESSION["addTheme"] = "<div class='alert alert-primary'><strong>Oops !</strong> Nom déja existant !</div>";
            header("Location: ../addTheme.php?idCate=" . $idCate . "");
            die();
        }


        mysqli_query($bdd, "INSERT INTO theme VALUES (NULL, '" . $nom . "', '" . $description . "', '" . $url . "', '" . $now . "', '" . $idCate . "', '" . $_SESSION["idProfil"] . "');");

        $_SESSION["addTheme"] = "<div class='alert alert-success'><strong>Yaas !</strong> Thème ajouté !</div>";
    } else {
        $_SESSION["addTheme"] = "<div class='alert alert-primary'><strong>Aïe !</strong> Une erreur interne s'est produite...</div>";
    }


}

// FIN IMG FIN IMG FIN IMG


header("Location: ../addTheme.php?idCate=" . $idCate);

