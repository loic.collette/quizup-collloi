<?php

require_once(__DIR__."/../../../php/database/connect.php");
require_once(__DIR__."/../../../php/functions/user.php");
require_once(__DIR__."/../../../php/functions/json.php");

if(!isLoggedIn()){
    die();
}

$answer = array();

if(!isset($_POST["nom"])){
    $answer["status"] = true;
    $answer["message"] = "<div class='alert alert-primary'><strong>Oops !</strong> Requête incorecte !</div>";
    die(encodeAndSendJson($answer));
}

if(empty($_POST["nom"])){
    $answer["status"] = true;
    $answer["message"] = "<div class='alert alert-primary'><strong>Oops !</strong> Nom vide !</div>";
    die(encodeAndSendJson($answer));
}

$categorie = addslashes(htmlspecialchars($_POST["nom"]));




$result = mysqli_query($bdd, "SELECT * FROM categorie WHERE libelleCategorie like '".$categorie."';");

if($result->num_rows >= 1){

    $answer["status"] = true;
    $answer["message"] = "<div class='alert alert-primary'><strong>Oops !</strong> Nom déja existant !</div>";
    die(encodeAndSendJson($answer));
}

$result = mysqli_query($bdd, "INSERT INTO categorie VALUES (NULL, '".$categorie."');");

if(mysqli_affected_rows($bdd)){


    $answer["status"] = true;
    $answer["message"] = "<div class='alert alert-success'><strong>Yes !</strong> Catégorie ajoutée !</div>";
    die(encodeAndSendJson($answer));

}else{

    $answer["status"] = true;
    $answer["message"] = "<div class='alert alert-primary'><strong>Oops !</strong> Erreur lors de l'ajout... (".mysqli_error($bdd).")</div>";
    die(encodeAndSendJson($answer));
}