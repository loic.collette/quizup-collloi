<?php
/**
 * Created by PhpStorm.
 * User: collloi
 * Date: 11/03/19
 * Time: 12:28
 *
 *
 */


require_once(__DIR__."/../../php/functions/user.php");
require_once(__DIR__."/../../php/functions/themes.php");

if(!isset($_GET["idTheme"])){
    header("Location: /game/");
    die();
}

$idTheme = htmlspecialchars(addslashes($_GET["idTheme"]));

if(!doesThisThemeExist($idTheme)){
    header("Location: /game/");
    die();
}


@session_start();

redirectIfnotLoggedIn();

?>

<html>
<head>
    <title>Ajouter un thème</title>
    <?php require(__DIR__."/../../inc/head.php"); ?>
</head>
<body>

<?php require(__DIR__."/../../inc/nav.php"); ?>

<section>

    <header>
        <h2>Ajouter une question</h2>
    </header>

    <article>

        <div id="serverAnswer">
            <?php echo(@$_SESSION["addQuestion"]); $_SESSION["addQuestion"] = ""; ?>
        </div>


        <form action="./ajax/addQuestion.php" method="post" enctype="multipart/form-data">


            <div class="form-group">
                <label class="col-form-label col-form-label" for="question">Nouvelle question</label>
                <input class="form-control form-control" type="text" placeholder="Nouvelle question" id="question" name="question">
            </div>

            <div class="form-group">
                <label class="col-form-label col-form-label" for="goodAnswer">Bonne réponse</label>
                <input class="form-control form-control" type="text" placeholder="Bonne réponse" id="goodAnswer" name="goodAnswer">
            </div>
            <div class="form-group">
                <label class="col-form-label col-form-label" for="badAnswer1">Mauvaise réponse 1</label>
                <input class="form-control form-control" type="text" placeholder="Mauvaise réponse 1" id="badAnswer1" name="badAnswer1">
            </div>
            <div class="form-group">
                <label class="col-form-label col-form-label" for="badAnswer2">Mauvaise réponse 2</label>
                <input class="form-control form-control" type="text" placeholder="Mauvaise réponse 2" id="badAnswer2" name="badAnswer2">
            </div>
            <div class="form-group">
                <label class="col-form-label col-form-label" for="badAnswer3">Mauvaise réponse 3</label>
                <input class="form-control form-control" type="text" placeholder="Mauvaise réponse 3" id="badAnswer3" name="badAnswer3">
            </div>

            <input name="idTheme" hidden  value="<?php echo($_GET["idTheme"]); ?>" />

            <div class="custom-control custom-switch">
                <input type="checkbox" class="custom-control-input" name="isImage" id="isImage">
                <label class="custom-control-label" for="isImage">Image dans la question ?</label>
            </div>


            <div class="form-group">
                <div class="input-group mb-3">
                    <div class="custom-file">
                        <input type="file" class="custom-file-input" id="imgQuestion" name="imgQuestion">
                        <label class="custom-file-label" for="imgQuestion">Image de la question (facultatif)</label>
                    </div>
                </div>
            </div>


            <input type="submit" class="btn btn-success addTheme" value="Ajouter">

        </form>

    </article>


</section>


<?php include(__DIR__."/../../inc/footer.php"); ?>

</body>
</html>
