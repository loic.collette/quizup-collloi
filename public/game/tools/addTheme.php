<?php
/**
 * Created by PhpStorm.
 * User: collloi
 * Date: 11/03/19
 * Time: 12:28
 *
 *
 */


require_once(__DIR__."/../../php/functions/user.php");
require_once(__DIR__."/../../php/functions/themes.php");

if(!isset($_GET["idCate"])){
    header("Location: /game/");
    die();
}

$idCate = htmlspecialchars(addslashes($_GET["idCate"]));

if(!doesThisCategorieExist($idCate)){
    header("Location: /game/");
    die();
}


@session_start();

redirectIfnotLoggedIn();

?>

<html>
<head>
    <title>Ajouter un thème</title>
    <?php require(__DIR__."/../../inc/head.php"); ?>
</head>
<body>

<?php require(__DIR__."/../../inc/nav.php"); ?>

<section>

    <header>
        <h2>Ajouter un thème</h2>
    </header>

    <article>

        <div id="serverAnswer">
            <?php echo(@$_SESSION["addTheme"]); @$_SESSION["addTheme"] = ""; ?>
        </div>


        <form action="./ajax/addTheme.php" method="post" enctype="multipart/form-data">


            <div class="form-group">
                <label class="col-form-label col-form-label" for="nom">Nom du nouveau thème</label>
                <input class="form-control form-control" type="text" placeholder="Nom du nouveau thème" id="nom" name="nom">
            </div>

            <div class="form-group">
                <label class="col-form-label col-form-label" for="description">Description du nouveau thème</label>
                <input class="form-control form-control" type="text" placeholder="Description du nouveau thème" id="description" name="description">
            </div>

            <input name="idCate" hidden value="<?php echo($_GET["idCate"]); ?>" />

            <div class="form-group">
                <div class="input-group mb-3">
                    <div class="custom-file">
                        <input type="file" class="custom-file-input" id="logoTheme" name="logoTheme">
                        <label class="custom-file-label" for="logoTheme">Logo du thème (128 px x 128 px)</label>
                    </div>
                </div>
            </div>


        <input type="submit" class="btn btn-success addTheme" value="Ajouter">

        </form>

    </article>


</section>


<?php include(__DIR__."/../../inc/footer.php"); ?>

</body>
</html>
