<?php
/**
 * Created by PhpStorm.
 * User: collloi
 * Date: 27/03/19
 * Time: 18:46
 */

@session_start();
require_once(__DIR__ . "/../../php/functions/user.php");
require_once(__DIR__ . "/../../php/functions/game.php");


$answer = array(
    "status" => true,
    "message" => ""
);

if (!isLoggedIn()) {
    $answer["message"] = "Not connected.";
    die(json_encode($answer));

}

if (!isset($_POST["idRep"]) || !isAnswerAllowed($_POST["idRep"])) {

    $answer["message"] = "Contenu invalide.";
    die(json_encode($answer));
}

$userReponse = addslashes(htmlspecialchars($_POST["idRep"]));

if(isEndGame()){

    $answer = array(
        "status" => true,
        "message" => "Déja la fin du jeu.",
        "endGame" => true
    );
    die(json_encode($answer));
}


$points = 0;
if(checkAnswer($userReponse)){
    $points = calculPoints();
    $answer["details"]["isValid"] = true;
    $answer["details"]["points"] = $points;

}else{
    $answer["details"]["isValid"] = false;
    $answer["details"]["points"] = $points;
}

$answer["details"]["totalPoints"] = $_SESSION["game"]["totalPoints"];

insertUserAnswer($userReponse, $points);


if(isEndGame()){
    $answer["endGame"] = true;
}else{
    $answer["endGame"] = false;
    updateLastTime();
    $id = updateNextQuestion();




    if(!isEndGame()) {
        $answer["answer"]["idQuestion"] = $id +1;
        $answer["isImage"] = false;
        if($_SESSION["game"]["content"][$id]["image"] != NULL){

            $answer["isImage"] = true;
            $answer["imgQuestion"] = $_SESSION["game"]["content"][$id]["image"];
        }
        $answer["textQuestion"] = $_SESSION["game"]["content"][$id]["question"];

        $answer["answer"]["nextQuestion"] = pickNextQuestion();
    }else{

        $answer["endGame"] = true;
        $answer["newIdPartie"] = sendAllGameDetails();
    }
}

echo(json_encode($answer));