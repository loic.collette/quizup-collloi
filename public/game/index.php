<?php
/**
 * Created by PhpStorm.
 * User: collloi
 * Date: 12/03/19
 * Time: 12:54
 */


require_once(__DIR__."/../php/functions/user.php");

redirectIfnotLoggedIn();
?>

<html>
<head>
    <title>Accueil</title>
    <?php require(__DIR__ . "/../inc/head.php"); ?>
</head>
<body>

<?php require(__DIR__ . "/../inc/nav.php"); ?>


<section>
    <div class="jumbotron">
        <h1 class="display-3">Catégories</h1>
        <p class="lead">Ici vous trouverez tout les catégories des thèmes.</p>
        <hr class="my-4">
        <p>Choisissez une catégorie, cliquez sur <span class="badge badge-success">Voir</span>, choisissez un thème et commencez une partie !</p>
        <p>
            <a class="btn btn-success" href="./tools/addCategorie.php">Ajouter</a>
        </p>
    </div>
</section>
<section>
    <header>
        <h2>Catégories disponibles</h2>
    </header>

    <article>

        <table class="table table-hover">
            <thead>
            <tr>
                <th scope="col">Nom</th>
                <th scope="col">Thèmes</th>
                <th scope="col">Voir</th>
            </tr>
            </thead>
            <tbody>

            <?php

            $categories = getAllCategories();


            foreach ($categories as $categorie) {
                $nbTheme = countThemeByCategorieId($categorie["id"]);
                ?>

                <tr>
                    <td><?php echo($categorie["nom"]); ?></td>
                    <td><?php echo($nbTheme); ?> thème(s)</td>
                    <?php if ($nbTheme <= 0) { ?>
                        <td><a class="btn btn-danger txtWhite" href="./tools/addTheme.php?idCate=<?php echo($categorie["id"]); ?>">Ajouter</a></td>
                    <?php } else if ($nbTheme > 0 and $nbTheme < 5) { ?>
                        <td>
                            <a class="btn btn-success txtWhite" href="./themes.php?idCate=<?php echo($categorie["id"]); ?>">Voir</a>
                            <a class="btn btn-warning txtWhite" href="./tools/addTheme.php?idCate=<?php echo($categorie["id"]); ?>">Ajouter</a>
                        </td>
                    <?php } else { ?>
                        <td><a class="btn btn-success txtWhite" href="./themes.php?idCate=<?php echo($categorie["id"]); ?>">Voir</a></td>
                    <?php } ?>
                </tr>

            <?php } ?>
            </tbody>
        </table>

    </article>

</section>

<?php include(__DIR__ . "/../inc/footer.php"); ?>

</body>
</html>
