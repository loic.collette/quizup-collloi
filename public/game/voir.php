<?php
/**
 * Created by PhpStorm.
 * User: collloi
 * Date: 19/03/19
 * Time: 21:11
 */

require_once(__DIR__ . "/../php/functions/user.php");
require_once(__DIR__ . "/../php/functions/social.php");
redirectIfnotLoggedIn();

if (!isset($_GET["idTheme"])) {
    header("Location: /game/");
    die();
}
$idTheme = addslashes(htmlspecialchars($_GET["idTheme"]));
require_once(__DIR__ . "/../php/functions/themes.php");
$theme = getThemeDetailsById($idTheme);

if (!$theme["success"]) {
    header("Location: /game/");
    die();
}

@session_start();


?>

<html>
<head>
    <title>Accueil</title>
    <?php require(__DIR__ . "/../inc/head.php"); ?>
</head>
<body>

<?php require(__DIR__ . "/../inc/nav.php"); ?>


<section>

    <div id="statusServer">

    </div>

    <div class="jumbotron" style="background: linear-gradient(0.30turn, #ffffff, #eb6460);">
        <div class="themePic" style="background-image: url(<?php echo($theme["details"]["logo"]); ?>)">

        </div>
        <h1 class="display-3"><?php echo($theme["details"]["nom"]); ?></h1>
        <p class="lead"><?php echo($theme["details"]["description"]); ?></p>
        <hr class="my-4">
        <p>
            Créé par <a><?php echo($theme["details"]["idProfil"]["details"]["username"]); ?></a>, mis à jour
            le <?php echo(explode(" ", $theme["details"]["lastUpdate"])[0]); ?>
            à <?php echo(explode(" ", $theme["details"]["lastUpdate"])[1]); ?>.
        </p>
        <hr class="my-4">
        <p>
            <a class="btn btn-success"
               href="./php/startGame.php?idTheme=<?php echo($theme["details"]["id"]); ?>">Jouer</a>

            <?php if (isLoggedIn()) { ?>
                <?php if (doIfollowThisTheme($idTheme)){ ?>
                <a class="btn btn-success unfollow" href="#">Ne plus suivre</a>

                <script>
                    $(".unfollow").click(function () {
                        $.post("./ajax/unfollow.php",
                            {
                                idTheme: <?php echo($idTheme); ?>,
                            },

                            function (data, status) {

                                data = JSON.parse(data);

                                if (data["success"] === true) {
                                    sleep(2000).then(() => {
                                        // Do something after the sleep!
                                        window.location.replace("./voir.php?idTheme=" + <?php echo($idTheme); ?>);
                                    });

                                }
                                document.getElementById("statusServer").innerHTML = data["message"];


                            }
                        );
                    });
                </script>

            <?php }else{ ?>
                <a class="btn btn-success follow" href="#">Suivre</a>

                <script>
                    $(".follow").click(function () {
                        $.post("./ajax/follow.php",
                            {
                                idTheme: <?php echo($idTheme); ?>,
                            },

                            function (data, status) {

                                data = JSON.parse(data);

                                if (data["success"] === true) {
                                    sleep(2000).then(() => {
                                        // Do something after the sleep!
                                        window.location.replace("./voir.php?idTheme=" + <?php echo($idTheme); ?>);
                                    });

                                }
                                document.getElementById("statusServer").innerHTML = data["message"];


                            }
                        );
                    });
                </script>

            <?php } ?>

                <?php if(doesThisThemeBelongToThisUser($idTheme)){ ?>

                <a class="btn btn-danger" href="tools/addQuestion.php?idTheme=<?php echo($idTheme); ?>">Ajouter des
                    questions</a>
                <a class="btn btn-danger" href="tools/addTitre.php?idTheme=<?php echo($idTheme); ?>">Ajouter des
                    titres</a>

                    <?php } ?>

            <?php } ?>

        </p>
        <h2>Top 5 des joueurs</h2>
        <table class="table table-hover">
            <thead>
            <tr>
                <td>
                    Position #
                </td>
                <td>
                    Nom d'utilisateur
                </td>
                <td>
                    Niveau
                </td>
                <td>
                    Voir
                </td>
            </tr>

            </thead>
            <tbody>
            <?php


            $classement = getTop5PlaysFromThemeId($idTheme);

            $i = 1;

            foreach ($classement as $joueur) {

                ?>
                <tr>

                    <td>
                        <?php echo($i); ?>
                    </td>
                    <td>
                        <?php echo($joueur["username"]); ?>
                    </td>
                    <td>
                        <?php echo($joueur["points"]); ?>
                    </td>
                    <td>
                        <a class="btn btn-sm btn-success" href="/social/profile.php?idProfil=<?php echo($joueur["id"]); ?>">Voir</a>
                    </td>

                </tr>

            <?php $i++; } ?>
            </tbody>
        </table>
    </div>
</section>
<section>
    <header>
        <h2>Fil d'actualité du thème</h2>
    </header>

    <article>
        <div class="post">
            <div class="commentSection">
                <h5>Votre post</h5>
                <div class="row">
                    <div class="col-xs-12 col-sm-3 col-md-10">

                        <input id="text-reponse--1" class="form-control comment-Text"/>
                    </div>
                    <div class="col-md-2">
                        <button onclick="sendComment(-1)" class="comment-Send btn btn-outline-primary">
                            Envoyer
                        </button>

                    </div>
                </div>
            </div>
        </div>

        <br/>

        <?php


        $result = mysqli_query($bdd, "SELECT * FROM message JOIN profil p on message.idProfil = p.idProfil WHERE idTheme = " . $idTheme . " AND idMessage_1 IS NULL ORDER BY idMessage DESC;");

        if ($result->num_rows == 0) {
            ?>

            <div class="alert alert-warning"><strong>Oops... </strong> Aucun post sur ce thème...
            </div>

            <?php
        } else {

            while ($row = mysqli_fetch_array($result)) {
                ?>

                <div class="post">
                    <div class="card mb-3">
                        <h3 class="card-header">Post de <?php echo($row["nomProfil"]); ?>
                        </h3>

                        <div class="card-body">

                            <p class="card-text"><?php echo($row["contenuMessage"]); ?></p>

                            <small><span
                                        id="like-<?php echo($row["idMessage"]); ?>"><?php echo(getNbLikeOnMessageById($row["idMessage"])); ?></span>
                                j'aime
                            </small>
                        </div>

                        <div class="card-body">
                            <?php if (doILikeThis($row["idMessage"])) { ?>
                                <a class="btn btn-outline-success" id="<?php echo($row["idMessage"]); ?>"
                                   onclick="likeMessage(<?php echo($row["idMessage"]); ?>)">Je n'aime plus</a>
                            <?php } else { ?>
                                <a class="btn btn-outline-primary" id="<?php echo($row["idMessage"]); ?>"
                                   onclick="likeMessage(<?php echo($row["idMessage"]); ?>)">J'aime </a>
                            <?php } ?>


                        </div>

                        <div class="card-body">

                            <h4>Réponses du post</h4>
                            <div class="commentSection">
                                <h5>Votre commentaire</h5>
                                <div class="row">
                                    <div class="col-xs-12 col-sm-3 col-md-10">

                                        <input id="text-reponse-<?php echo($row["idMessage"]); ?>"
                                               class="form-control comment-Text  "/>
                                    </div>
                                    <div class="col-md-2">
                                        <button onclick="sendComment(<?php echo($row["idMessage"]); ?>)"
                                                class="comment-Send btn btn-outline-primary">
                                            Envoyer
                                        </button>

                                    </div>
                                </div>
                            </div>
                            <br/>
                            <?php

                            $reponses = mysqli_query($bdd, "SELECT * FROM message JOIN profil p on message.idProfil = p.idProfil WHERE idMessage_1 IS NOT NULL AND idMessage_1 = " . $row["idMessage"] . " ORDER BY idMessage DESC;");

                            if ($reponses->num_rows == 0) {

                                ?>

                                <div class="alert alert-warning"><strong>Oops... </strong> Aucune réponse...
                                </div>

                                <?php

                            } else {

                                while ($rowRep = mysqli_fetch_array($reponses)) {
                                    ?>

                                    <div class="list-group-item list-group-item-action flex-column align-items-start">
                                        <div class="d-flex w-100 justify-content-between">
                                            <h5 class="mb-1"><?php echo($rowRep["nomProfil"]); ?>
                                            </h5>
                                            <small class="text-muted"><?php echo(explode(" ", $rowRep["timestampMessage"])[0]); ?>
                                                à <?php echo(explode(" ", $rowRep["timestampMessage"])[1]); ?></small>
                                        </div>
                                        <p class="mb-1"><?php echo($rowRep["contenuMessage"]); ?></p>
                                        <small><span
                                                    id="like-<?php echo($rowRep["idMessage"]); ?>"><?php echo(getNbLikeOnMessageById($rowRep["idMessage"])); ?></span>
                                            j'aime
                                        </small>
                                        <div class="card-body">
                                            <?php if (doILikeThis($rowRep["idMessage"])) { ?>
                                                <a class="btn btn-outline-success btn-sm"
                                                   id="<?php echo($rowRep["idMessage"]); ?>"
                                                   onclick="likeMessage(<?php echo($rowRep["idMessage"]); ?>)">Je n'aime
                                                    plus
                                                </a>

                                            <?php } else { ?>
                                                <a class="btn btn-outline-primary btn-sm"
                                                   id="<?php echo($rowRep["idMessage"]); ?>"
                                                   onclick="likeMessage(<?php echo($rowRep["idMessage"]); ?>)">J'aime
                                                </a>
                                            <?php } ?>

                                        </div>

                                        <div class="card-body">

                                            <h4>En réponse à ce commentaire</h4>

                                            <div class="commentSection">
                                                <h5>Votre commentaire</h5>
                                                <div class="row">
                                                    <div class="col-xs-12 col-sm-3 col-md-10">

                                                        <input id="text-reponse-<?php echo($rowRep["idMessage"]); ?>"
                                                               class="form-control comment-Text"/>
                                                    </div>
                                                    <div class="col-md-2">
                                                        <button onclick="sendComment(<?php echo($rowRep["idMessage"]); ?>)"
                                                                class="comment-Send btn btn-outline-primary">
                                                            Envoyer
                                                        </button>

                                                    </div>
                                                </div>
                                            </div>
                                            <br/>

                                            <?php

                                            $reponsesRep = mysqli_query($bdd, "SELECT * FROM message JOIN profil p on message.idProfil = p.idProfil WHERE idMessage_1 IS NOT NULL AND idMessage_1 = " . $rowRep["idMessage"] . " ORDER BY idMessage DESC;");

                                            if ($reponsesRep->num_rows == 0) {

                                                ?>

                                                <div class="alert alert-warning"><strong>Oops... </strong> Aucune
                                                    réponse...
                                                </div>

                                                <?php

                                            } else {

                                                while ($rowRepRep = mysqli_fetch_array($reponsesRep)) {
                                                    ?>

                                                    <div class="list-group-item list-group-item-action flex-column align-items-start">
                                                        <div class="d-flex w-100 justify-content-between">
                                                            <h5 class="mb-1"><?php echo($rowRepRep["nomProfil"]); ?>
                                                            </h5>
                                                            <small class="text-muted"><?php echo(explode(" ", $rowRepRep["timestampMessage"])[0]); ?>
                                                                à <?php echo(explode(" ", $rowRepRep["timestampMessage"])[1]); ?></small>
                                                        </div>
                                                        <p class="mb-1"><?php echo($rowRepRep["contenuMessage"]); ?></p>
                                                        <small><span
                                                                    id="like-<?php echo($rowRepRep["idMessage"]); ?>"><?php echo(getNbLikeOnMessageById($rowRepRep["idMessage"])); ?></span>
                                                            j'aime
                                                        </small>
                                                        <div class="card-body">
                                                            <?php if (doILikeThis($rowRepRep["idMessage"])) { ?>
                                                                <a id="<?php echo($rowRepRep["idMessage"]); ?>"
                                                                   class="btn btn-outline-success btn-sm"
                                                                   onclick="likeMessage(<?php echo($rowRepRep["idMessage"]); ?>)">Je
                                                                    n'aime plus</a>
                                                            <?php } else { ?>
                                                                <a id="<?php echo($rowRepRep["idMessage"]); ?>"
                                                                   class="btn btn-outline-primary btn-sm"
                                                                   onclick="likeMessage(<?php echo($rowRepRep["idMessage"]); ?>)">J'aime</a>
                                                            <?php } ?>

                                                        </div>


                                                    </div>

                                                <?php }
                                            } ?>


                                        </div>


                                    </div>

                                <?php }
                            } ?>


                        </div>
                        <div class="card-footer text-muted">
                            Posté le <?php echo(explode(" ", $row["timestampMessage"])[0]); ?>
                            à <?php echo(explode(" ", $row["timestampMessage"])[1]); ?>
                        </div>
                    </div>
                </div>

            <?php }
        } ?>


    </article>

</section>

<script>


    var likeMsg = new Audio('/assets/like.ogg');
    var dislikeMsg = new Audio('/assets/dislike.ogg');


    function likeMessage(idMessage) {
        $.post("/social/ajax/like.php",
            {
                idMessage: idMessage,
            },

            function (data, status) {

                data = JSON.parse(data);

                if (data["success"] === true) {

                    document.getElementById(idMessage).textContent = data["newText"];
                    document.getElementById("like-" + idMessage).textContent = data["nbLike"];

                    if (data["newText"] === "Je n'aime plus") {

                        likeMsg.play();
                    } else {

                        dislikeMsg.play();
                    }

                }


            }
        );
    }

    function sendComment(idMessage) {
        $.post("/social/ajax/comment.php",
            {
                idMessage: idMessage,
                idTheme: <?php echo($idTheme); ?>,
                contenu: document.getElementById("text-reponse-" + idMessage).value,
            },

            function (data, status) {

                data = JSON.parse(data);

                if (data["success"] === true) {
                    if (data["refresh"] === true) {
                        location.reload();
                    }
                }
                alert(data["alert"]);


            }
        );
    }


</script>

<?php include(__DIR__ . "/../inc/footer.php"); ?>

</body>
</html>

