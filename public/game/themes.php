<?php
/**
 * Created by PhpStorm.
 * User: collloi
 * Date: 19/03/19
 * Time: 21:11
 */


require_once(__DIR__ . "/../php/functions/themes.php");

require_once(__DIR__."/../php/functions/user.php");
if (!isset($_GET["idCate"])) {
    header("Location: /game/");
    die();
}



$idCate = addslashes(htmlspecialchars($_GET["idCate"]));

if(!doesThisCategorieExist($idCate)){


    header("Location: /game/");
    die();

}

@session_start();
redirectIfnotLoggedIn();
?>

<html>
<head>
    <title>Accueil</title>
    <?php require(__DIR__ . "/../inc/head.php"); ?>
</head>
<body>

<?php require(__DIR__ . "/../inc/nav.php"); ?>


<section>
    <div class="jumbotron">
        <h1 class="display-3">Thèmes</h1>
        <p class="lead">Ici vous trouverez tout les thèmes des jeux.</p>
        <hr class="my-4">
        <p>Choisissez un thème, cliquez sur <span class="badge badge-success">Voir</span> et commencez une partie !</p>
        <p>
            <a class="btn btn-success" href="./tools/addTheme.php?idCate=<?php echo($idCate); ?>">Ajouter</a>
        </p>
    </div>
</section>
<section>
    <header>
        <h2>Thèmes disponibles</h2>
    </header>

    <article>

        <table class="table table-hover">
            <thead>
            <tr>
                <th scope="col">Image</th>
                <th scope="col">Nom</th>
                <th scope="col">Questions</th>
                <th scope="col">Voir</th>
            </tr>
            </thead>
            <tbody>

            <?php

            $categories = getAllThemeByCategorieId($idCate);

            ?>

            <?php
            foreach ($categories as $categorie) {
                $nbQuest = countQuestionByThemeId($categorie["id"]);
                ?>

                <tr>
                    <td><img class="logoThemeList" src="<?php echo($categorie["logo"]); ?>" alt="<?php echo($categorie["nom"]); ?>"/></td>
                    <td><?php echo($categorie["nom"]); ?></td>
                    <td><?php echo($nbQuest); ?> question(s)</td>

                        <td>
                            <a class="btn btn-success txtWhite" href="./voir.php?idTheme=<?php echo($categorie["id"]); ?>">Voir !</a>
                            <?php if(doesThisThemeBelongToThisUser($categorie["id"])){ ?>
                            <a class="btn btn-warning txtWhite" href="./tools/addQuestion.php?idTheme=<?php echo($categorie["id"]); ?>">Ajouter</a>
                            <?php } ?>
                        </td>

                </tr>

            <?php } ?>
            </tbody>
        </table>

    </article>

</section>

<?php include(__DIR__ . "/../inc/footer.php"); ?>

</body>
</html>

