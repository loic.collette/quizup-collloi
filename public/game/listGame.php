<?php
/**
 * Created by PhpStorm.
 * User: collloi
 * Date: 11/03/19
 * Time: 12:28
 *
 *
 */

require_once(__DIR__."/../php/database/connect.php");
require_once(__DIR__."/../php/functions/user.php");
require_once(__DIR__."/../php/functions/themes.php");

@redirectIfnotLoggedIn();

if (isset($_GET["idProfil"])) {
    $idProfile = addslashes(htmlspecialchars($_GET["idProfil"]));
} else if (isLoggedIn()) {
    $idProfile = addslashes(htmlspecialchars($_SESSION["idProfil"]));
} else {
    header("Location: /");
    die();
}


$result = mysqli_query($bdd, "SELECT * FROM partie JOIN integrer i on partie.idPartie = i.idPartie JOIN repondre r on partie.idPartie = r.idPartie JOIN question q on r.idQuestion = q.idQuestion AND q.idQuestion = i.idQuestion JOIN participer p on partie.idPartie = p.idPartie WHERE p.idProfil = ".$idProfile." GROUP BY p.idPartie ORDER BY p.idPartie DESC");
$userData = getProfileDetails($idProfile);

?>

<html>
<head>
    <title>Liste de parties</title>
    <?php require(__DIR__."/../inc/head.php"); ?>
</head>
<body>

<?php require(__DIR__."/../inc/nav.php"); ?>

<section>
    <header>
        <h2>
            Liste de toutes les parties de <?php echo($userData["details"]["username"]); ?>
        </h2>
        <article>

            <?php


            while($row = mysqli_fetch_array($result)){ ?>

            <div class="card">
                <div class="card-body">
                    <h4 class="card-title"><?php echo(getProfileDetails($row["idProfil"])["details"]["username"]); ?></h4>
                    <h6 class="card-subtitle mb-2 text-muted"><?php echo(getThemeDetailsById($row["idTheme"])["details"]["nom"]); ?></h6>
                    <p class="card-text"><?php echo($row["timestampPartie"]); ?></p>
                    <a href="./result.php?idPartie=<?php echo($row["idPartie"]); ?>&fromList=true" class="card-link">Voir la partie</a>
                </div>
            </div>
                <br />

            <?php } ?>

        </article>
    </header>
</section>



<?php include(__DIR__."/../inc/footer.php"); ?>

</body>
</html>
